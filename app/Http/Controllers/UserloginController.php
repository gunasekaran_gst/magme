<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserloginController extends Controller
{
    public function index()
    {
        return view('logins');
    }

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|unique :register',
            'pwd' => 'required',

        ]);
    }

    public function insert(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'pwd' => 'required',
        ]);

        if ($login = DB::table('register')->where('email', $request->email)->where('pwd', $request->pwd)->first()) {

            $email = $request->email;
            return view('index', compact('email','login'));

        } else {

            return redirect()->back()->with('message', ' The password that you\'ve entered is incorrect.');
//            return view('hi');
        }

    }

//    public function check1(Request $request)
//    {
//        $users = DB::table('userlogin')->where('email', $request->email)->where('pwd', $request->pwd)->get();
//        return view('paidbooking')->with('users', $users);
//
//    }

}
