<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class RegController extends Controller
{
    public function insertform()
    {
        return view('reg');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' =>  'required|unique :register',
            'pwd' => 'required',
            'conpwd' => 'required',

        ]);
    }


    public function insert(Request $request)
    {
        $username_exist = DB::table('register')->where('email','=',$request->email)->first();

        if($username_exist){
            return redirect()->back()->with('msg', 'Email address already exists');

        }
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'pwd' => 'required',
            'conpwd' => 'required',

        ]);


        $name = $request->name;
        $email = $request->email;
        $pwd = $request->pwd;
        $conpwd = $request->conpwd;



        DB::insert('insert into register ( name, email, pwd, conpwd) values(?,?,?,?)', [$name, $email, $pwd, $conpwd]);

        return redirect()->back()->with ('message','Register success ');


    }



}