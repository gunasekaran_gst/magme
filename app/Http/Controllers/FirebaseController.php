<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Kreait\Firebase;

use Kreait\Firebase\Factory;

use Kreait\Firebase\ServiceAccount;

use Kreait\Firebase\Database;

class FirebaseController extends Controller

{

//

    public function index()
    {

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/laravelwithfirebase-6ae34-firebase-adminsdk-t9rdp-3e1319b872.json');

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://laravelwithfirebase-6ae34.firebaseio.com/')
            ->create();

        $db = $firebase->getDatabase();



        $db->getReference('0000')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "8610",
            "courseId" => "0004",
            "courseName" => "BANKING-IBPS,SBI,RRB,LIC,IPPS",
            "courseOffer" => "40",
            "paidAmt" => "8610",
            "receiptNo" => "0000",
            "regDate" => "08-05-2018",
            "remainAmt" => "0",
            "stuMob" => "9590520000",
            "txnId" => "1525759131873"
        ]);
        $db->getReference('0001')->set([
            "adminId" => "MagSob",
            "branchCode" => "MTSAKABS014",
            "courseFee" => "8610",
            "courseId" => "0004",
            "courseName" => "BANKING-IBPS,SBI,RRB,LIC,IPPS",
            "courseOffer" => "40",
            "paidAmt" => "8610",
            "receiptNo" => "0000",
            "regDate" => "08-05-2018",
            "remainAmt" => "0",
            "stuMob" => "9590520000",
            "txnId" => "1525759131873"
        ]);


        echo '<h1>Data as inserted in Firebase</h1>';

    }

}

?>