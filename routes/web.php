<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('mage');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/hi', function () {
    return view('hi');
});


//Route::get('/auth/login', function () {
//    return view('auth/login');
//});

Route::get('/login', function () {
    return view('login');
});
Route::get('logins','UserloginController@index');
Route::post('logins','UserloginController@insert');

Route::get('admin','AdminloginController@index');
Route::post('admin','AdminloginController@insert');

Route::get('reg','RegController@insertform');
Route::post('reg','RegController@insert');

Route::get('serial','SerialController@index');
Route::post('serial','SerialController@insert');


Route::get('/index', function () {
    return view('index');
});

Route::get('/magme_course', function () {
    return view('magme_course');
});

Route::get('/magme_events', function () {
    return view('magme_events');
});

Route::get('/magme_student', function () {
    return view('magme_student');
});

Route::get('/magme_branchdetails', function () {
    return view('magme_branchdetails');
});

Route::get('/magme_coursedetails', function () {
    return view('magme_coursedetails');
});

Route::get('/magme_eventsdetails', function () {
    return view('magme_eventsdetails');
});


Route::get('/magme_studentdetails', function () {
    return view('magme_studentdetails');
});

Route::get('/branch_ed', function () {
    return view('branch_ed');
});


Route::get('/exam', function () {
    return view('exam');
});

Route::get('/home', function () {
    return view('home');
});


Route::get('/firebase', function () {
    return view('firebase');
});


Route::get('/account', function () {
    return view('account');
});


Route::get('/demo', function () {
    return view('demo');
});


Route::get('/coursedemo', function () {
    return view('coursedemo');
});


Route::get('/eventsdemo', function () {
    return view('eventsdemo');
});


Route::get('/studentdemo', function () {
    return view('studentdemo');
});

Route::get('/dispatch', function () {
    return view('dispatch');
});


Route::get('/coursedetails', function () {
    return view('coursedetails');
});

Route::get('/branchdetails', function () {
    return view('branchdetails');
});


Route::get('/eventsdetails', function () {
    return view('eventsdetails');
});

Route::get('/studentdetails', function () {
    return view('studentdetails');
});

Route::get('/firebase_login', function () {
    return view('firebase_login');
});


Route::get('/branch_login', function () {
    return view('branch_login');
});


Route::get('/course', function () {
    return view('course');
});

Route::get('/magme_dateview', function () {
    return view('magme_dateview');
});

Route::get('/update', function () {
    return view('update');
});

Route::get('/admin_branch', function () {
    return view('admin_branch');
});
Route::get('/admin_course', function () {
    return view('admin_course');
});
Route::get('/admin_event', function () {
    return view('admin_event');
});
Route::get('/admin_student', function () {
    return view('admin_student');
});
Route::get('/admin_date', function () {
    return view('admin_date');
});
Route::get('/magmelogin_student', function () {
    return view('magmelogin_student');
});

Route::get('/magmelogin_event', function () {
    return view('magmelogin_event');
});

Route::get('/magmelogin_studetails', function () {
    return view('magmelogin_studetails');
});

Route::get('/regs', function () {
    return view('regs');
});


Route::get('/table', function () {
    return view('table');
});


Route::get('/phpfirebase_sdk','FirebaseController@index');

Route::get('users', 'HomesController@users');

Route::get('course', 'CourseController@index');

Route::get('events', 'EventsController@index');

Route::get('student', 'StudentController@index');


//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
