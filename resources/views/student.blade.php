<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:21 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Mosaddek" />
    <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="javascript:;" type="image/png">

    <title>Magme - Responsive Admin Dashboard Template</title>

    <!--easy pie chart-->
    <link href="js/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />

    <!--vector maps -->
    <link rel="stylesheet" href="js/vector-map/jquery-jvectormap-1.1.1.css">


    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">

    <!--switchery-->
    <link href="js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />

    <!--jquery-ui-->
    <link href="js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />

    <!--iCheck-->
    <link href="js/icheck/skins/all.css" rel="stylesheet">

    <link href="css/owl.carousel.css" rel="stylesheet">


    <!--common style-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>


    <![endif]-->
</head>



<body class="sticky-header">

<section>
    <!-- sidebar left start-->
    <div class="sidebar-left">
        <!--responsive view logo start-->
        <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
            <a href="">
                <!--<i class="fa fa-maxcdn"></i>-->
                <span class="brand-name">Magme</span>
            </a>
        </div>
        <!--responsive view logo end-->

        <div class="sidebar-left-info">
            <!-- visible small devices start-->
            <div class=" search-field">  </div>
            <!-- visible small devices end-->

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked side-Navigation">
                <li>
                    <h3 class="Navigation-title">Created Of The Leader</h3>
                </li>
                <li class="active"><a href=""><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Register</span></a>
                    <ul class="child-list">
                        <li><a href="#">Branch Register</a></li>
                        <li><a href="magme_course">Course Register</a></li>
                        <li><a href="magme_events">Events Register</a></li>
                        <li><a href="magme_student">Student Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>View Details</span></a>
                    <ul class="child-list">
                        <li><a href="magme_branchdetails">Branch Register</a></li>
                        <li><a href="magme_coursedetails">Course Register</a></li>
                        <li><a href="magme_eventsdetails">Events Register</a></li>
                        <li><a href="magme_studentdetails">Student Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Magme Edit/Delete</span></a>
                    <ul class="child-list">
                        <li><a href="demo">Branch Register</a></li>
                        <li><a href="coursedemo">Course Register</a></li>
                        <li><a href="eventsdemo">Events Register</a></li>
                        <li><a href="studentdemo">Student Register</a></li>
                    </ul>
                </li>

            </ul>
            <!--sidebar nav end-->

            <!--sidebar widget start-->

            <!--sidebar widget end-->

        </div>
    </div>
    <!-- sidebar left end-->
    <p id="demo"></p>

    <script>
        function myFunction() {
            var x, text;
            x = document.getElementById("numb").value;
            if (isNaN(x) || x < 11 || x > 13) {
                text = "Input not valid";
            } else {
                text = "Input OK";
            }
            document.getElementById("demo").innerHTML = text;
        }
    </script>

    <script>
        function validateForm() {
            var x = document.forms["myForm"]["fname"].value;
            if (x == "") {
                alert("Name must be filled out");
                return false;
            }
        }
    </script>


    <!-- body content start-->
    <div class="body-content" >
        <div class="col-lg-8 padd_top_30">
            <section class="panel">
                <header class="panel-heading">
                    Branch
                </header>

                <div class="panel-body">
                    <form name="myForm"
                          onsubmit="return validateForm()" method="post">
                        <div class="form-group">
                            <div class="form-group">

                                <label for="exampleInputEmail1">Branch Name</label>
                                <input type="text" required name="branchName"class="form-control" id="branchName"  placeholder="branch name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Branch Code</label>
                            <input type="text" required name="branchCode" class="form-control" id="branchCode"  placeholder="branch code">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Branch Email</label>
                            <input type="text"required  name="branchEmail" class="form-control" id="branchEmail"  placeholder="email">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Branch Password</label>
                            <input type="text" required  name="branchPwd" class="form-control" id="branchPwd"  placeholder="passowrd">

                        </div>
                        <div class="padd_but">
                            <button type="submit">Click</button>
                            <label>Remember Me*</label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Image Url</label>
                            <progress id="uploader" value="0" max="100">0%</progress>
                            <input type="file" required id="fileButton" name="fileButton" value="upload"/>

                            <img id="myImg" src="#" alt="show image" height=100 width=100>
                        </div>

                    </form>

                    <div class="form-group">
                        <input type="button"  class="save_button" value="Save" onclick="uploadImage();" />
                        <a href="index"> <input type="button"  class="save_button" value="Cancle"/> </a>
                    </div>

                </div>

            </section>
        </div>

        <script>
            window.addEventListener('load', function() {
                document.querySelector('input[type="file"]').addEventListener('change', function() {
                    if (this.files && this.files[0]) {
                        var img = document.querySelector('img');  // $('img')[0]
                        img.src = URL.createObjectURL(this.files[0]); // set src to file url
                        img.onload = imageIsLoaded; // optional onload event listener
                    }
                });
            });

            function imageIsLoaded(e) { alert(e); }
        </script>


        <!-- header section start-->
        <div class="header-section">

            <!--logo and logo icon start-->
            <div class="logo dark-logo-bg hidden-xs hidden-sm">
                <a href="index">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">Magme</span>
                </a>
            </div>

            <!--logo and logo icon end-->

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
            <!--toggle button end-->

            <!--mega menu start-->
            <div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
                <ul class="nav navbar-nav">
                    <!-- Classic list -->
                    <li class="dropdown"><a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle">Magme Admission &nbsp;  <b
                                    class=" fa fa-angle-down"></b></a>
                        <ul class="dropdown-menu wide-full">
                            <li>
                                <!-- Content container to add padding -->
                                <div class="yamm-content">
                                    <div class="row">
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Register</p>
                                            </li>
                                            <li><a href="#"> Branch Register</a>
                                            </li>
                                            <li><a href="magme_course"> Course Register</a>
                                            </li>
                                            <li><a href="magme_events"> Events Register </a>
                                            </li>
                                            <li><a href="magme_student"> Student Register </a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Details</p>
                                            </li>
                                            <li><a href="magme_branchdetails"> Branch Details</a>
                                            </li>
                                            <li><a href="magme_coursedetails"> Course Details</a>
                                            </li>
                                            <li><a href="magme_eventsdetails"> Events Details </a>
                                            </li>
                                            <li><a href="magme_studentdetails"> Student Details </a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Edit/Delete</p>
                                            </li>
                                            <li><a href="demo"> Branch Edit/Delete</a>
                                            </li>
                                            <li><a href="coursedemo"> Course Edit/Delete</a>
                                            </li>
                                            <li><a href="eventsdemo"> Events Edit/Delete </a>
                                            </li>
                                            <li><a href="studentdemo"> Student Edit/Delete </a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title"> Datewise view</p>
                                            </li>
                                            <li><a href="magme_dateview">Student Details</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Classic dropdown -->
                </ul>
            </div>
            <!--mega menu end-->


            <!--right notification start-->
            <div class="right-notification">
                <ul class="notification-menu">


                    <li>
                        <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="" alt="">Log out
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                            <li><a href="javascript:;">  Profile</a></li>
                            <li>
                                <a href="javascript:;">
                                    <span class="badge bg-danger pull-right">40%</span>
                                    <span>Settings</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label bg-info pull-right">new</span>
                                    Help
                                </a>
                            </li>
                            <li><a href="logins"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <li>
                        <div class="sb-toggle-right">
                            <i class="fa fa-indent"></i>
                        </div>
                    </li>

                </ul>
            </div>
            <!--right notification end-->
        </div>

    </div>



    <!-- header section end-->

    <!-- page head start-->

    <!-- page head end-->

    <!--body wrapper start-->

    <!--body wrapper end-->


    <!--footer section start-->

    <!--footer section end-->


    <!-- Right Slidebar start -->
    <div class="sb-slidebar sb-right sb-style-overlay">
        <div class="right-bar">

            <span class="r-close-btn sb-close"><i class="fa fa-times"></i></span>

            <ul class="nav nav-tabs nav-justified-">
                <li class="">
                    <a href="#settings" data-toggle="tab">Magme Admission</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active " id="chat">
                    <ul>
                        <li>
                            <a href="index" data-toggle="tab">Branch Register</a>
                        </li>
                        <li>
                            <a href="magme_course" data-toggle="tab">Course Register</a>
                        </li>
                        <li>
                            <a href="magme_events" data-toggle="tab">Events Register</a>
                        </li>
                        <li>
                            <a href="magme_student" data-toggle="tab">Student Register</a>
                        </li>
                    </ul>


                </div>


            </div>
        </div>
    </div>

</section>




<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js">
</script>
<script>
    // Initialize Firebase

    var file;

    var config = {
        apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
        authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
        databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
        projectId: "laravelwithfirebase-6ae34",
        storageBucket: "laravelwithfirebase-6ae34.appspot.com",
        messagingSenderId: "871324337451"
    };


    firebase.initializeApp(config);

    var uploader = document.getElementById('uploader');
    var fileButton = document.getElementById('fileButton');
    fileButton.addEventListener('change', function(e){
        file = e.target.files[0];;
    });

    var tblUsers = document.getElementById('tbl_users_list');
    var databaseRef = firebase.database().ref('branch/');
    var rowIndex = 1;

    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblUsers.insertRow(rowIndex);
            var cellId = row.insertCell(0);
            var cellName = row.insertCell(1);
            var cellCode = row.insertCell(2);
            var cellEmail = row.insertCell(3);
            var cellPassword = row.insertCell(4);

            cellId.appendChild(document.createTextNode(childKey));
            cellName.appendChild(document.createTextNode(childData.branchName));
            cellCode.appendChild(document.createTextNode(childData.branchCode));
            cellEmail.appendChild(document.createTextNode(childData.branchEmail));
            cellPassword.appendChild(document.createTextNode(childData.branchPwd));


            rowIndex = rowIndex + 1;
        });
    });

    function save_user(lastid,imageUrl){
        var inc = lastid;
        var branchName = document.getElementById('branchName').value;
        var branchCode = document.getElementById('branchCode').value;
        var branchEmail = document.getElementById('branchEmail').value;
        var branchPwd = document.getElementById('branchPwd').value;
        var fileButton = imageUrl;


        var uid = "1234";
        var data = {
            inc: inc,
            branchName: branchName,
            branchCode: branchCode,
            branchEmail: branchEmail,
            branchPwd: branchPwd,
            fileButton: fileButton
        }

        var updates = {};
        updates['/branch/' + inc] = data;
        firebase.database().ref().update(updates);

        alert('The user is created successfully!');
        reload_page();
    }

    function getAllCourse(imageUrl) {

        var person = [];
        var id = 0;
        console.log(JSON.stringify(person));

        databaseRef.once('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();

                person.push(childKey);

            });
            person.sort();
            console.log(JSON.stringify(person));
            if (person.length>0)
            {
                id =person[person.length-1];
                id++;
            }
            id = pad(id,4);
            save_user(id,imageUrl);
        });
    }

    function uploadImage()
    {

        var storageRef = firebase.storage().ref('img/'+file.name);
        var task = storageRef.put(file);
        task.on('state_changed', function progress(snapshot) {
            var percentage = (snapshot.bytesTransferred/snapshot.totalBytes)*100;
            uploader.value = percentage;
            console.log("loded");

        }, function error(err) {
            console.log("erroer");


        },function() {
            console.log('File av');

            task.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                console.log('File available at', downloadURL);
                imageUrl = downloadURL;
                getAllCourse(imageUrl);
            });

        });
    }



    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }


    function reload_page(){
        window.location.reload();
    }
</script>
<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>

<!--jquery-ui-->
<script src="js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>

<script src="js/jquery-migrate.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>

<!--Nice Scroll-->
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

<!--right slidebar-->
<script src="js/slidebars.min.js"></script>

<!--switchery-->
<script src="js/switchery/switchery.min.js"></script>
<script src="js/switchery/switchery-init.js"></script>

<!--flot chart -->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/flot-spline.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.pie.js"></script>

<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.crosshair.js"></script>


<!--earning chart init-->
<script src="js/earning-chart-init.js"></script>


<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--easy pie chart-->
<script src="js/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="js/easy-pie-chart.js"></script>


<!--vectormap-->
<script src="js/vector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/vector-map/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/dashboard-vmap-init.js"></script>

<!--Icheck-->
<script src="js/icheck/skins/icheck.min.js"></script>
<script src="js/todo-init.js"></script>

<!--jquery countTo-->
<script src="js/jquery-countTo/jquery.countTo.js"  type="text/javascript"></script>

<!--owl carousel-->
<script src="js/owl.carousel.js"></script>

<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>



<!--common scripts for all pages-->

<script src="js/scripts.js"></script>


<script type="text/javascript">

    $(document).ready(function() {

        //countTo

        $('.timer').countTo();

        //owl carousel

        $("#news-feed").owlCarousel({
            Navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
    });

    $(window).on("resize",function(){
        var owl = $("#news-feed").data("owlCarousel");
        owl.reinit();
    });

</script>

<script type="text/javascript">
    <!--
    function validateEmail()
    {
        var emailID = document.myForm.EMail.value;
        atpos = emailID.indexOf("@");
        dotpos = emailID.lastIndexOf(".");

        if (atpos < 1 || ( dotpos - atpos < 2 ))
        {
            alert("Please enter correct email ID")
            document.myForm.EMail.focus() ;
            return false;
        }
        return( true );
    }
    //-->
</script>

{{--<footer>--}}
{{--2018&copy; Log Out Technologies.--}}
{{--</footer>--}}
</body>

<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:59 GMT -->
</html>
