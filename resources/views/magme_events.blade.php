<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:21 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Mosaddek" />
    <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="javascript:;" type="image/png">

    <title>Magme - Responsive Admin Dashboard Template</title>

    <!--easy pie chart-->
    <link href="js/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />

    <!--vector maps -->
    <link rel="stylesheet" href="js/vector-map/jquery-jvectormap-1.1.1.css">

    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">

    <!--switchery-->
    <link href="js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />

    <!--jquery-ui-->
    <link href="js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />

    <!--iCheck-->
    <link href="js/icheck/skins/all.css" rel="stylesheet">

    <link href="css/owl.carousel.css" rel="stylesheet">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


    <!--common style-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>


    <![endif]-->
</head>

<body class="sticky-header">


<section>
    <!-- sidebar left start-->
    <div class="sidebar-left">
        <!--responsive view logo start-->
        <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
            <a href="magme_events">
                <!--<i class="fa fa-maxcdn"></i>-->
                <span class="brand-name">Magme</span>
            </a>
        </div>
        <!--responsive view logo end-->

        <div class="sidebar-left-info">
            <!-- visible small devices start-->
            <div class=" search-field">  </div>
            <!-- visible small devices end-->

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked side-Navigation">
                <li>
                    <h3 class="Navigation-title">Created Of the Leader</h3>
                </li>
                <li class="active"><a href="magme_events"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Register</span></a>
                    <ul class="child-list">
                        <li><a href="http://127.0.0.1:8000/">Branch Register</a></li>
                        <li><a href="magme_course">Course Register</a></li>
                        <li><a href="magme_events">Events Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>View Details</span></a>
                    <ul class="child-list">
                        <li><a href="magme_branchdetails">Branch Register</a></li>
                        <li><a href="magme_coursedetails">Course Register</a></li>
                        <li><a href="magme_eventsdetails">Events Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Magme Edit/Delete</span></a>
                    <ul class="child-list">
                        <li><a href="demo">Branch </a></li>
                        <br>
                        <li><a href="coursedemo">Course </a></li>
                        <br>
                        <li><a href="eventsdemo">Events </a></li>
                    </ul>
                </li>

            </ul>
            <!--sidebar nav end-->

            <!--sidebar widget start-->
            <!-- search the particualr records-->

            <script>
                $(document).ready(function(){
                    $("#myInput").on("keyup", function() {
                        var value = $(this).val().toLowerCase();
                        $("#myTable tr").filter(function() {
                            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                    });
                });
            </script>
            <!--sidebar widget end-->

        </div>
    </div>
    <!-- sidebar left end-->
    <script>
        function validateForm() {
            var x = document.forms["myForm"]["fname"].value;
            if (x == "") {
                alert("Name must be filled out");
                return false;
            }
        }
    </script>
    <!-- body content start-->
    <div class="body-content" >
        <div class="col-lg-8 padd_top_30">
            <section class="panel">
                <header class="panel-heading">
                    Events
                </header>
                <div class="panel-body">
                    <form name="myForm"
                          onsubmit="return validateForm()" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Event Name</label>
                        <input type="text" name="eventName"class="form-control" id="eventName" required placeholder="event name">
                    </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Enter Branch Code</label>
                            <input type="text" name="toBranch" class="form-control" id="toBranch" required placeholder="enter the branch no ..">
                        </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Event Date</label>
                        <input type="date" name="eventDate"  class="form-control" id="eventDate" required placeholder="event date">
                    </div>
                        <div class="form-group">
                        <label for="exampleInputEmail1">Event Descuription</label>
                        <input type="text" name="eventDesc" class="form-control" id="eventDesc" required placeholder="Description">
                    </div>

                    </form>
                    <div class="form-group">
                        <input type="button"  class="save_button" value="Save" onclick="getAllCourse();"/>&nbsp;&nbsp;&nbsp;
                        <a href="magme_events"> <input type="button"  class="save_button" value="Clear"/> </a>
                    </div>

                </div>
            </section>
        </div>

        <div class="col-lg-4 padd_top_30">
            <br>
            <input class="form-control" id="myInput" type="text" placeholder="Branch name Search........">
            <br>
            <table id="tbl_course_list" class="table" border="1">
                <thead>
                <tr>
                    <td style="background-color: #2F6F9F">Branch Code</td>
                    <td style="background-color: #2F6F9F">Branch NAME</td>
                </tr>
                </thead>
                <tbody id="myTable">
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
            </table>

        </div>
        <!-- header section start-->
        <div class="header-section">

            <!--logo and logo icon start-->
            <div class="logo dark-logo-bg hidden-xs hidden-sm">
                <a href="magme_events">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">Magme</span>
                </a>
            </div>

            <!--logo and logo icon end-->

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
            <!--toggle button end-->

            <!--mega menu start-->
            <div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
                <ul class="nav navbar-nav">
                    <!-- Classic list -->
                    <li class="dropdown"><a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle">Magme Admission &nbsp;  <b
                                    class=" fa fa-angle-down"></b></a>
                        <ul class="dropdown-menu wide-full">
                            <li>
                                <!-- Content container to add padding -->
                                <div class="yamm-content">
                                    <div class="row">
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Register</p>
                                            </li>
                                            <li><a href="index"> Branch Register</a>
                                            </li>
                                            <li><a href="magme_course"> Course Register</a>
                                            </li>
                                            <li><a href="magme_events"> Events Register </a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Details</p>
                                            </li>
                                            <li><a href="magme_branchdetails"> Branch Details</a>
                                            </li>
                                            <li><a href="magme_coursedetails"> Course Details</a>
                                            </li>
                                            <li><a href="magme_eventsdetails"> Events Details </a>
                                            </li>

                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Edit/Delete</p>
                                            </li>
                                            <li><a href="demo"> Branch</a>
                                            </li>
                                            <br>
                                            <li><a href="coursedemo"> Course</a>
                                            </li>
                                            <br>
                                            <li><a href="eventsdemo"> Events</a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title"> Datewise view</p>
                                            </li>
                                            <li><a href="magme_dateview">Student dateview</a>
                                            </li>
                                            <br>
                                            <li><a href="magme_studentdetails">Admission Details Download</a>
                                            </li>
                                            <br>
                                            <li><a href="magmelogin_studetails">Student Details Download</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <!-- Classic dropdown -->

                </ul>
            </div>
            <!--mega menu end-->


            <!--right notification start-->
            <div class="right-notification">
                <ul class="notification-menu">
                    <li>
                        <form class="search-content" action="http://thevectorlab.net/slicklab/index.html" method="post">
                            <input type="text" class="form-control" name="keyword" placeholder="Search...">
                        </form>
                    </li>

                    <li>
                        <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="img/avatar-mini.jpg" alt="">Log Out
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                            <li><a href="logins"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <li>
                        <div class="sb-toggle-right">
                            <i class="fa fa-indent"></i>
                        </div>
                    </li>

                </ul>
            </div>
            <!--right notification end-->
        </div>
            <!--mega menu end-->


            <!--right notification start-->

            <!--right notification end-->
        </div>
    </div>



    <!-- header section end-->

    <!-- page head start-->

    <!-- page head end-->

    <!--body wrapper start-->

    <!--body wrapper end-->


    <!--footer section start-->

    <!--footer section end-->


    <!-- Right Slidebar start -->
    <div class="sb-slidebar sb-right sb-style-overlay">
        <div class="right-bar">

            <span class="r-close-btn sb-close"><i class="fa fa-times"></i></span>

            <ul class="nav nav-tabs nav-justified-">
                <li class="">
                    <a href="#settings" data-toggle="tab">Magme Admission</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active " id="chat">
                    <ul>
                        <li>
                            <a href="" data-toggle="tab">Branch Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Course Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Events Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Student Register</a>
                        </li>
                    </ul>


                </div>


            </div>
        </div>
    </div>

</section>



<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>

<!--jquery-ui-->
<script src="js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>

<script src="js/jquery-migrate.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>

<!--Nice Scroll-->
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

<!--right slidebar-->
<script src="js/slidebars.min.js"></script>

<!--switchery-->
<script src="js/switchery/switchery.min.js"></script>
<script src="js/switchery/switchery-init.js"></script>

<!--flot chart -->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/flot-spline.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.pie.js"></script>

<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.crosshair.js"></script>


<!--earning chart init-->
<script src="js/earning-chart-init.js"></script>


<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--easy pie chart-->
<script src="js/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="js/easy-pie-chart.js"></script>


<!--vectormap-->
<script src="js/vector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/vector-map/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/dashboard-vmap-init.js"></script>

<!--Icheck-->
<script src="js/icheck/skins/icheck.min.js"></script>
<script src="js/todo-init.js"></script>

<!--jquery countTo-->
<script src="js/jquery-countTo/jquery.countTo.js"  type="text/javascript"></script>

<!--owl carousel-->
<script src="js/owl.carousel.js"></script>

<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>


<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script>
    // Initialize Firebase

    var config = {
        apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
        authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
        databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
        projectId: "laravelwithfirebase-6ae34",
        storageBucket: "laravelwithfirebase-6ae34.appspot.com",
        messagingSenderId: "871324337451"
    };


    firebase.initializeApp(config);
</script>

<script>


    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblevents.insertRow(rowIndex);
            var eventid = row.insertCell(0);
            var celleventName = row.insertCell(1);
            var toBranch = row.insertCell(2);
            var celleventDate = row.insertCell(3);
            var celleventDesc = row.insertCell(4);

            eventid.appendChild(document.createTextNode(childKey));
            celleventName.appendChild(document.createTextNode(childData.eventName));
            toBranch.appendChild(document.createTextNode(childData.toBranch));
            celleventDate.appendChild(document.createTextNode(childData.eventDate));
            celleventDesc.appendChild(document.createTextNode(childData.eventDesc));


            rowIndex = rowIndex + 1;
        });
    });

    function save_user(lastid){

        var eventId = lastid;
        var eventName = document.getElementById('eventName').value;
        var toBranch = document.getElementById('toBranch').value;
        var eventDate = document.getElementById('eventDate').value;
        var eventDesc = document.getElementById('eventDesc').value;

        if(eventName == '' || toBranch == '' || eventDate == '' || eventDesc == '') {
            alert('Pleace enter miss filed');

        }else {

            var uid = "1234"
            var data = {
                eventId: eventId,
                eventName: eventName,
                toBranch: toBranch,
                eventDate: eventDate,
                eventDesc: eventDesc,
                adminId: "MagSob"

            }

            var updates = {};
            updates['/Event/' + eventId] = data;
            firebase.database().ref().update(updates);

            alert('The user is created successfully!');
            reload_page();
        }
    }

    function getAllCourse() {

        // console.log("getcourse");
        var person = [];
        var id = 0;
        console.log(JSON.stringify(person));

        var databaseRef = firebase.database().ref('Event/');
        databaseRef.once('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();

                person.push(childKey);

            });
            person.sort();
            console.log(JSON.stringify(person));
            if (person.length>0)
            {
                id =person[person.length-1];
                id++;
            }
            id = pad(id,4);
            save_user(id);
        });
    }
    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }



    function reload_page(){
        window.location.reload();
    }

</script>


<script>

    var tblcourse = document.getElementById('tbl_course_list');
    var databaseRef = firebase.database().ref('Branch/');
    var rowIndex = 1;

    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblcourse.insertRow(rowIndex);

            var cellId = row.insertCell(0);
            var cellName = row.insertCell(1);

            cellId.appendChild(document.createTextNode(childData.branchCode));
            cellName.appendChild(document.createTextNode(childData.branchName));

            rowIndex = rowIndex + 1;
        });
    });

    function reload_page(){
        window.location.reload();
    }

</script>

<!--common scripts for all pages-->

<script src="js/scripts.js"></script>


<script type="text/javascript">

    $(document).ready(function() {

        //countTo

        $('.timer').countTo();

        //owl carousel

        $("#news-feed").owlCarousel({
            Navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
    });

    $(window).on("resize",function(){
        var owl = $("#news-feed").data("owlCarousel");
        owl.reinit();
    });

</script>
{{--<footer>--}}
{{--2018&copy; Log Out Technologies.--}}
{{--</footer>--}}
</body>

<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:59 GMT -->
</html>
