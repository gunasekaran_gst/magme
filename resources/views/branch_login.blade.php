
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords">
    <meta name="description">
    <title>Magme</title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>

    <link rel="stylesheet" href="css/style.css">
    <link href="//fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Capture login form  Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />

</head>

<body>
<div class="container-fluid header">
    <div class="container-fluid menu">
        <nav class="navbar-inverse custom_class" data-spy="affix" data-offset-top="150">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle side_menu_toggle" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo col-lg-12 col-md-12 col-sm-12 col-xs-9">
                        <a href="{{ url('/') }}"> <img src="{{URL::asset('images/mag_learn_2.png') }}" width="260px" height="50px"> </a>
                    </div>
                </div>
                <div class="meanu">
                    <div class="collapse navbar-collapse topmenu" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a  href="{{ url('/reg') }}">REGISTER</a></li>

                            <li>
                                <a href="logins">
                                    <span class="slide_menu" style="font-size:17px; cursor:pointer">ADMIN LOGIN</span>
                                </a>
                            </li>
                            <li><a  href="{{ url('/branch_login') }}">ADMISSION</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container-fluid  home_bgs ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container">
            <form  data-wow-delay=".2s" class="wow bounceInDown">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="w3layouts">
                    <div class="signin-agile">
                        <h3> Branch log In</h3>

                            <input type="username" id="branchCodeEt"  class="name" placeholder="branch code..." required="">

                            <input type="password" id="branchPwdEt"  class="password" placeholder="branch password..." required="">

                        <ul>
                            <li>
                                <input type="checkbox" id="brand1">
                                <label for="brand1"><span></span>Remember me</label>
                            </li>
                        </ul>
                        <a  class="forgot" href="#">Forgot Password?</a><br>
                        <div class="clear"></div>
                       <input type="button"  value="Login" onclick="checkLogin();"/>

                    </div>

                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>



<script src="https://code.jquery.com/jquery-1.8.3.js"></script>
<script src="https://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" />
<script>
    // Initialize Firebase

    var config = {
        apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
        authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
        databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
        projectId: "laravelwithfirebase-6ae34",
        storageBucket: "laravelwithfirebase-6ae34.appspot.com",
        messagingSenderId: "871324337451"
    };

    firebase.initializeApp(config);

</script>
<script>

    function checkLogin() {

        var enteredCode = document.getElementById('branchCodeEt').value;
        var enteredPwd = document.getElementById('branchPwdEt').value;

        var login = false;

        databaseRef.once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();

                console.log(childData.branchCode);
                console.log(childData.branchPwd);


                console.log("---"+enteredCode);
                console.log("----"+enteredPwd);

                var code = childData.branchCode;
                var branchPwd = childData.branchPwd;

                console.log("--db--"+code);
                console.log("--db--"+branchPwd);


                var codecompare = code.localeCompare(enteredCode);
                var pwdcompare = branchPwd.localeCompare(enteredPwd);


                console.log("--check--"+codecompare);
                console.log("--check--"+pwdcompare);

                if (codecompare == 0
                    && pwdcompare == 0) {
                        login = true;
                }

            });

            if (login)
            {
               window.location="http://127.0.0.1:8000/update";

            }
            else {
                alert("falied");
            }

        });
    }



</script>




<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script>
    // Initialize Firebase

    var config = {
        apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
        authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
        databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
        projectId: "laravelwithfirebase-6ae34",
        storageBucket: "laravelwithfirebase-6ae34.appspot.com",
        messagingSenderId: "871324337451"
    };

    firebase.initializeApp(config);

</script>
<script>


    var databaseRef = firebase.database().ref('Branch/');
    var rowIndex = 1;

    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblusers.insertRow(rowIndex);
            var cellId = row.insertCell(0);
            var cellName = row.insertCell(1);
            var cellEmail = row.insertCell(2);
            var cellPassword = row.insertCell(3);

            cellId.appendChild(document.createTextNode(childData.branchCode));
            cellName.appendChild(document.createTextNode(childData.branchName));
            cellEmail.appendChild(document.createTextNode(childData.branchEmail));
            cellPassword.appendChild(document.createTextNode(childData.branchPwd));


            rowIndex = rowIndex + 1;
        });
    });

    function save_user(){
        var branchName = document.getElementById('branchName').value;
        var branchCode = document.getElementById('branchCode').value;
        var branchEmail = document.getElementById('branchEmail').value;
        var branchPwd = document.getElementById('branchPwd').value;
        var branchImage = document.getElementById('branchImage').value;

        var uid = firebase.database().ref().child('branch').push().key;

        var data = {
            user_id: uid,
            branchName: branchName,
            branchCode: branchCode,
            branchEmail: branchEmail,
            branchPwd: branchPwd,
            branchImage: branchImage
        }

        var updates = {};
        updates['/Branch/' + uid] = data;
        firebase.database().ref().update(updates);

        alert('The user is created successfully!');
        reload_page();
    }

    function update_user(){
        var branchName = document.getElementById('branchName').value;
        var branchCode = document.getElementById('branchCode').value;
        var branchEmail = document.getElementById('branchEmail').value;
        var branchPwd = document.getElementById('branchPwd').value;
        var user_id = document.getElementById('user_id').value;

        var data = {
            user_id: user_id,
            branchName: branchName,
            branchCode: branchCode,
            branchEmail: branchEmail,
            branchPwd: branchPwd
        }

        var updates = {};
        updates['/users/' + user_id] = data;
        firebase.database().ref().update(updates);

        alert('The user is updated successfully!');

        reload_page();
    }

    function delete_user(){
        var user_id = document.getElementById('user_id').value;

        firebase.database().ref().child('/users/' + user_id).remove();
        alert('The user is deleted successfully!');
        reload_page();
    }

    function reload_page(){
        window.location.reload();
    }

</script>


