
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords">
    <meta name="description">
    <title>Magme</title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>

    <link rel="stylesheet" href="css/style.css">
    <link href="//fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Capture login form  Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />

</head>
<body>
<div class="container-fluid header">
    <div class="container-fluid menu">
        <nav class="navbar-inverse custom_class" data-spy="affix" data-offset-top="150">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle side_menu_toggle" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo col-lg-12 col-md-12 col-sm-12 col-xs-9">
                        <a href="{{ url('/') }}"> <img src="{{URL::asset('images/mag_learn_2.png') }}" width="260px" height="50px"> </a>
                    </div>
                </div>
                <div class="meanu">
                    <div class="collapse navbar-collapse topmenu" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a  href="{{ url('/reg') }}">REGISTER</a></li>

                            <li>
                                <a href="logins">
                                    <span class="slide_menu" style="font-size:17px; cursor:pointer">LOG IN</span>
                                </a>
                            </li>
                            <li><a  href="{{ url('/admin') }}">ADMIN LOGIN</a></li>
                            <li><a  href="{{ url('/branch_login') }}">ADMISSION</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container-fluid  home_bgs ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container">
            <form action="/admin" method="post" data-wow-delay=".2s" class=" wow bounceInUp">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="w3layouts">

                    <div class="signin-agile">
                        <h3>Welcome</h3>
                        <div>
                            <a href="{{ url('/') }}"> <img src="{{URL::asset('images/login_mobile.png') }}" width="100pxpx" height="100px"> </a>
                        </div>


                        <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="username" name="email" class="name" placeholder="Email" required="">
                            @if ($errors->has('email'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif

                            <center style="color:white;">
                                @if(session()->has('message'))
                                    <div class="alertmessage">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                            </center>


                        </fieldset>
                        <fieldset class="{{ $errors->has('pwd') ? ' has-error' : '' }}">
                            <input type="password" name="pwd" class="password" placeholder="Password" required="">
                            @if ($errors->has('pwd'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('pwd') }}</strong></span>@endif

                        </fieldset>
                        <ul>
                            <li>
                                <input type="checkbox" id="brand1">
                                <label for="brand1"><span></span>Remember me</label>
                            </li>
                        </ul>
                        <a  class="forgot" href="#">Forgot Password?</a><br>
                        <div class="clear"></div>
                        <input type="submit" value="Login">
                    </div>

                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
</html>

