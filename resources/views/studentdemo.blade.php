<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>firebase by seegatesite.com</title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
</head>
<body bgcolor="gray">
<div class="container-fluid signin-agile">
    <div class="container">
        <header class="modal-card-head">
            <p class="modal-card-title">Magme Admission: Student</p>
            <div class="back_page"><a class="back_page" href="magmelogin_studetails">Back</a> </div>
        </header>
        <div id="card-list" class="row is-mobile">
        </div>
    </div>
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <section class="section">
                <div id="modal" class="modal">
                    <div class="modal-backgrounds"></div>
                    <div class="modal-card">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Magme Admission: Student</p>
                            <button class="btnClose delete" aria-label="close"></button>
                        </header>
                        <section class="modal-card-body">
                            <div class="field">
                                <label class="label">Student Name</label>
                                <div class="control">
                                    <input type="hidden" id="txtType">
                                    <input type="hidden" id="txtKey">
                                    <input class="input" id="stuName" type="text" placeholder="stuName">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Mobile No</label>
                                <div class="control">
                                    <input class="input" id="stuMob" type="text" placeholder="stuMob">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Email</label>
                                <div class="control">
                                    <input class="input" id="stuEmail" type="text" placeholder="stuEmail">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label"> Father Name</label>
                                <div class="control">
                                    <input class="input" id="stuFather" type="text" placeholder="stuFather">
                                </div>
                                <p class="help"></p>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button id="btnSave" class="button is-success">Save changes</button>
                            <button id="btnClose" class="button">Cancel</button>
                        </footer>

                    </div>
                </div>
            </section>
            <script src="https://code.jquery.com/jquery-2.2.4.min.js"
                    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                    crossorigin="anonymous"></script>
            <script src="https://www.gstatic.com/firebasejs/4.10.0/firebase.js"></script>
            <script>
                var nextkey = 0;
                var config = {
                    apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
                    authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
                    databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
                    projectId: "laravelwithfirebase-6ae34",
                    storageBucket: "laravelwithfirebase-6ae34.appspot.com",
                    messagingSenderId: "871324337451"
                };

                firebase.initializeApp(config);
                var database = firebase.database();

                database.ref('StudentDetails').on('child_added', function (data) {
                    add_data_table(data.val().stuName, data.val().stuMob, data.val().stuEmail, data.val().stuFather, data.key);
                    var lastkey = data.key;
                    nextkey = parseInt(lastkey) + 1;
                });
                database.ref('StudentDetails').on('child_changed', function (data) {
                    update_data_table(data.val().stuName, data.val().stuMob, data.val().stuEmail, data.val().stuFather, data.key)
                });
                database.ref('StudentDetails').on('child_removed', function (data) {
                    remove_data_table(data.key)
                });

                var tblstudent = document.getElementById('tbl_student_list');
                var databaseRef = firebase.database().ref('StudentDetails/');
                var rowIndex = 1;
                databaseRef.once('value', function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        var childKey = childSnapshot.key;
                        var childData = childSnapshot.val();

                        var row = tblstudent.insertRow(rowIndex);
                        var cellId = row.insertCell(0);
                        var stuName = row.insertCell(1);
                        var coursestuMob = row.insertCell(2);
                        var stuEmail = row.insertCell(3);
                        var stuFather = row.insertCell(4);

                        cellId.appendChild(document.createTextNode(childKey));
                        stuName.appendChild(document.createTextNode(childData.stuName));
                        coursestuMob.appendChild(document.createTextNode(childData.stuMob));
                        stuEmail.appendChild(document.createTextNode(childData.stuEmail));
                        stuFather.appendChild(document.createTextNode(childData.stuFather));


                        rowIndex = rowIndex + 1;
                    });
                });

                function add_data_table(stuName, stuMob, stuEmail, stuFather, key) {
                    $("#card-list").append('<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" id="' + key + '"><div class="card col-md-12 col-sm-12 col-xs-12 col-lg-12 edit_delete padding_left_right_null">' +
                        '<div class="card-image"><div class="media"><div class="media-content">' +
                        '<p class="branch">Student Name: ' + stuName + '</p>' +
                        '</div></div></div><footer class="card-footer">' +
                        '<a href="#" data-key="' + key + '" class="card-footer-item btnEdit">Edit</a>' +
                        '<a href="#" class="card-footer-item btnRemove"  data-key="' + key + '">Remove</a></footer></div></div>');
                }


                function remove_data_table(key) {
                    $("#card-list #" + key).remove();
                }

                function new_data(stuName, stuMob, stuEmail, stuFather, key) {
                    database.ref('StudentDetails/' + key).set({
                        stuName: stuName,
                        stuMob: stuMob,
                        stuEmail: stuEmail,
                        stuFather: stuFather
                    });
                }

                function update_data(stuName, stuMob, stuEmail, stuFather, key) {
                    database.ref('StudentDetails/' + key).update({
                        stuName: stuName,
                        stuMob: stuMob,
                        stuEmail: stuEmail,
                        stuFather: stuFather
                    });
                }

                $("#btnAdd").click(function () {
                    $("#stuName").val("");
                    $("#stuMob").val("");
                    $("#stuEmail").val("");
                    $("#stuFather").val("");
                    $("#txtType").val("N");
                    $("#txtKey").val("0");
                    $("#modal").addClass("is-active");

                });
                $("#btnSave").click(function () {
                    if ($("#txtType").val() == 'N') {
                        database.ref('StudentDetails').once("value").then(function (snapshot) {
                            if (snapshot.numChildren() == 0) {
                                nextkey = 1;
                            }
                            new_data($("#stuName").val(), $("#stuMob").val(),  $("#stuEmail").val(), $("#stuFather").val(), nextkey);
                        });
                    } else {
                        update_data($("#stuName").val(), $("#stuMob").val(),  $("#stuEmail").val(), $("#stuFather").val(), $("#txtKey").val());
                    }
                    $("#btnClose").click();
                });
                $(document).on("click", ".btnEdit", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('StudentDetails/' + key).once("value").then(function (snapshot) {
                        $("#stuName").val(snapshot.val().stuName);
                        $("#stuMob").val(snapshot.val().stuMob);
                        $("#stuEmail").val(snapshot.val().stuEmail);
                        $("#stuFather").val(snapshot.val().stuFather);
                        $("#txtType").val("E");
                        $("#txtKey").val(key);
                    });
                    $("#modal").addClass("is-active");
                });
                $(document).on("click", ".btnRemove", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('StudentDetails/' + key).remove();
                })

                $("#btnClose,.btnClose").click(function () {
                    $("#modal").removeClass("is-active");
                });
            </script>
        </div>
    </div>
</div>

</body>
</html>




































































