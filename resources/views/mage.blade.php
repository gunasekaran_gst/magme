<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords">
    <meta name="description">
    <title>Magme</title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>
</head>
<body>
<div class="container-fluid header">
    <div class="container-fluid menu">
        <nav class="navbar-inverse custom_class" data-spy="affix" data-offset-top="150">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle side_menu_toggle" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo col-lg-12 col-md-12 col-sm-12 col-xs-9">
                        <a href="{{ url('/') }}"> <img src="{{URL::asset('images/mag_learn_2.png') }}" width="260pxpx" height="50px"> </a>
                    </div>
                </div>

                <div class="meanu">
                    <div class="collapse navbar-collapse topmenu" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a  href="{{ url('/reg') }}">REGISTER</a></li>

                            <li>
                                <a href="logins">
                                    <span class="slide_menu" style="font-size:17px; cursor:pointer">ADMIN LOGIN</span>
                                </a>
                            </li>
                            <li><a  href="{{ url('/branch_login') }}">ADMISSION</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
    <div class="container-fluid  home_bg">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 professionals">
                    <h2 data-wow-delay=".3s" class="wow bounceInLeft"><strong>Real Training for Real People by Real Professionals!</strong></h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_bottom_ten_em">
                    <a class="col-lg-5 col-md-5 col-sm-5 col-xs-12 browse_course " href="{{ url('/') }}">
                        {{--<button data-wow-delay=".5s" class="browser courses_button wow bounceInUp">ADMISSION</button>--}}
                    </a>

                </div>
            </div>
        </div>
    </div>


</body>
</html>

