<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>firebase by seegatesite.com</title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
</head>
<body bgcolor="#333333">
<div class="container-fluid">
    <div class="container">
        <header class="modal-card-head">
            <p class="modal-card-title">Magme Admission: Course</p>
            <div class="back_page"><a class="back_page" href="magme_coursedetails">Back</a> </div>
        </header>

        <div id="card-list" class="row is-mobile">

        </div>
    </div>
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <section class="section">
                <div id="modal" class="modal">
                    <div class="modal-backgrounds"></div>
                    <div class="modal-card">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Magme Admission: Course</p>
                            <button class="btnClose delete" aria-label="close"></button>
                        </header>
                        <section class="modal-card-body">
                            <div class="field">
                                <label class="label">Course Name</label>
                                <div class="control">
                                    <input type="hidden" id="txtType">
                                    <input type="hidden" id="txtKey">
                                    <input class="input" id="courseName" type="text" placeholder="courseName">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">No of Duration</label>
                                <div class="control">
                                    <input class="input" id="courseDuration" type="text" placeholder="image url">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Fess</label>
                                <div class="control">
                                    <input class="input" id="courseFee" type="text" placeholder="courseFee">
                                </div>
                                <p class="help"></p>
                            </div>

                            <div class="field">
                                <label class="label">Offer</label>
                                <div class="control">
                                    <input class="input" id="courseOffer" type="text" placeholder="courseOffer">
                                </div>
                                <p class="help"></p>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button id="btnSave" class="button is-success">Save changes</button>
                            <button id="btnClose" class="button">Cancel</button>
                        </footer>
                    </div>
                </div>
            </section>
            <script src="https://code.jquery.com/jquery-2.2.4.min.js"
                    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                    crossorigin="anonymous"></script>
            <script src="https://www.gstatic.com/firebasejs/4.10.0/firebase.js"></script>
            <script>
                var nextkey = 0;
                var config = {
                    apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
                    authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
                    databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
                    projectId: "laravelwithfirebase-6ae34",
                    storageBucket: "laravelwithfirebase-6ae34.appspot.com",
                    messagingSenderId: "871324337451"
                };

                firebase.initializeApp(config);
                var database = firebase.database();

                database.ref('Course').on('child_added', function (data) {
                    add_data_table(data.val().courseName, data.val().courseDuration, data.val().courseFee, data.val().courseName, data.val().courseOffer, data.key);
                    var lastkey = data.key;
                    nextkey = parseInt(lastkey) + 1;
                });
                database.ref('Course').on('child_changed', function (data) {
                    update_data_table(data.val().courseName, data.val().courseDuration, data.val().courseFee, data.val().courseName, data.val().courseOffer, data.key)
                });
                database.ref('Course').on('child_removed', function (data) {
                    remove_data_table(data.key)
                });

                var tblcourse = document.getElementById('tbl_course_list');
                var databaseRef = firebase.database().ref('Course/');
                var rowIndex = 1;

                databaseRef.once('value', function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        var childKey = childSnapshot.key;
                        var childData = childSnapshot.val();

                        var row = tblcourse.insertRow(rowIndex);
                        var cellId = row.insertCell(0);
                        var courseName = row.insertCell(1);
                        var sbranch = row.insertCell(2);
                        var courseDuration = row.insertCell(3);
                        var courseFee = row.insertCell(4);
                        var courseOffer = row.insertCell(5);

                        cellId.appendChild(document.createTextNode(childKey));
                        courseName.appendChild(document.createTextNode(childData.courseName));
                        sbranch.appendChild(document.createTextNode(childData.sbranch));
                        courseDuration.appendChild(document.createTextNode(childData.courseDuration));
                        courseFee.appendChild(document.createTextNode(childData.courseFee));
                        courseOffer.appendChild(document.createTextNode(childData.courseOffer));

                        rowIndex = rowIndex + 1;
                    });
                });

                function reload_page(){
                    window.location.reload();
                }

                function add_data_table(courseName, branchImage, courseDuration, courseOffer, courseFee, key) {
                    $("#card-list").append('<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" id="' + key + '"><div class="card col-md-12 col-sm-12 col-xs-12 col-lg-12 edit_delete padding_left_right_null">' +
                        '<div class="card-image"><div class="media"><div class="media-content">' +
                        '<p class="branch">Course Name: ' + courseName + '</p>' +
                        '</div></div></div><footer class="card-footer">' +
                        '<a href="#" data-key="' + key + '" class="card-footer-item btnEdit">Edit</a>' +
                        '<a href="#" class="card-footer-item btnRemove"  data-key="' + key + '">Remove</a></footer></div></div>');
                }



                function remove_data_table(key) {
                    $("#card-list #" + key).remove();
                }

                function new_data(courseName, courseDuration, courseOffer, courseFee,  key) {
                    database.ref('Course/' + key).set({
                        courseName: courseName,
                        courseDuration: courseDuration,
                        courseOffer: courseOffer,
                        courseFee: courseFee,
                    });
                }

                function update_data(courseName, courseDuration, courseOffer, courseFee, key) {
                    database.ref('Course/' + key).update({
                        courseName: courseName,
                        courseDuration: courseDuration,
                        courseOffer: courseOffer,
                        courseFee: courseFee,

                    });
                }

                $("#btnAdd").click(function () {
                    $("#courseName").val("");
                    $("#courseDuration").val("");
                    $("#courseOffer").val("");
                    $("#courseFee").val("");
                    $("#txtType").val("N");
                    $("#txtKey").val("0");
                    $("#modal").addClass("is-active");

                });
                $("#btnSave").click(function () {
                    if ($("#txtType").val() == 'N') {
                        database.ref('Course').once("value").then(function (snapshot) {
                            if (snapshot.numChildren() == 0) {
                                nextkey = 1;
                            }
                            new_data($("#courseName").val(), $("#courseDuration").val(), $("#courseFee").val(), $("#courseOffer").val(), nextkey);
                        });
                    } else {
                        update_data($("#courseName").val(), $("#courseDuration").val(), $("#courseFee").val(),  $("#courseOffer").val(), $("#txtKey").val());
                    }
                    $("#btnClose").click();
                });
                $(document).on("click", ".btnEdit", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('Course/' + key).once("value").then(function (snapshot) {
                        $("#courseName").val(snapshot.val().courseName);
                        $("#courseDuration").val(snapshot.val().courseDuration);
                        $("#courseOffer").val(snapshot.val().courseOffer);
                        $("#courseFee").val(snapshot.val().courseFee);
                        $("#txtType").val("E");
                        $("#txtKey").val(key);
                    });
                    $("#modal").addClass("is-active");
                });
                $(document).on("click", ".btnRemove", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('Course/' + key).remove();
                })

                $("#btnClose,.btnClose").click(function () {
                    $("#modal").removeClass("is-active");
                });
            </script>
        </div>
    </div>
</div>

</body>
</html>




































































