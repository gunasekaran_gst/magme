

<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords">
    <meta name="description">
    <title>Magme </title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>

    <link rel="stylesheet" href="css/style.css">
    <link href="//fonts.googleapis.com/css?family=Muli:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Capture login form  Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />

</head>
<body>
<div class="container-fluid header">
    <div class="container-fluid menu">
        <nav class="navbar-inverse custom_class" data-spy="affix" data-offset-top="150">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle side_menu_toggle" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo col-lg-12 col-md-12 col-sm-12 col-xs-9">
                        <a href="{{ url('/') }}"> <img src="{{URL::asset('images/mag_learn_2.png') }}" width="260px" height="50px"> </a>
                    </div>
                </div>

                <div class="meanu">
                    <div class="collapse navbar-collapse topmenu" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a  href="{{ url('/reg') }}">REGISTER</a></li>

                            <li>
                                <a href="logins">
                                    <span class="slide_menu" style="font-size:17px; cursor:pointer">ADMIN LOGIN</span>
                                </a>
                            </li>
                            <li><a  href="{{ url('/branch_login') }}">ADMISSION</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container-fluid  home_bgs">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container">


            <form action="/reg" method="post" data-wow-delay=".4s" class="wow zoomIn">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="w3layouts">
                    <div class="signin-agiles">
                        <center style="color:red;">
                            @if(session()->has('message'))
                                <div class="alertmessage">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                        </center>
                        <h3>Register</h3>
                        <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="username" id="name" name="name"
                                   placeholder="Enter the  name....." value="{{ old('name') }}">
                            @if ($errors->has('name'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                        </fieldset>


                        <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="username" id="email" name="email"
                                   placeholder="Enter the Username....." value="{{ old('email') }}">
                            @if ($errors->has('email'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                                <center style="color:red;">
                                    @if(session()->has('msg'))
                                        <div class="alertmessage">
                                            {{ session()->get('msg') }}
                                        </div>
                                    @endif
                                </center>

                        </fieldset>


                        <fieldset class="{{ $errors->has('pwd') ? ' has-error' : '' }}">
                            <input type="username" id="pwd" name="pwd"
                                   placeholder="Enter the password....." value="{{ old('pwd') }}">
                            @if ($errors->has('pwd'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('pwd') }}</strong></span>@endif
                        </fieldset>


                        <fieldset class="{{ $errors->has('conpwd') ? ' has-error' : '' }}">
                            <input type="username" id="conpwd" name="conpwd"
                                   placeholder="Enter the conform password....." value="{{ old('conpwd') }}">
                            @if ($errors->has('conpwd'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('conpwd') }}</strong></span>@endif
                        </fieldset>
                        <input type="submit" value="Submit"> <br>
                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
</html>








