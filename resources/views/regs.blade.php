<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>firebase by seegatesite.com</title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="container">
        <h1 class="title">
            Course Details
        </h1>
        <div id="card-list" class="image">
        </div>
    </div>
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <section class="section">
                <div id="modal" class="modal">
                    <div class="modal-backgrounds"></div>
                    <div class="modal-card">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Magme Admission: Branch</p>
                            <button class="btnClose delete" aria-label="close"></button>
                        </header>
                        <section class="modal-card-body">
                            <div class="field">
                                <label class="label">Course NAME</label>
                                <div class="control">
                                    <input type="hidden" id="txtType">
                                    <input type="hidden" id="txtKey">
                                    <input class="input" id="courseName" type="text" placeholder="branch_name">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Select Branch</label>
                                <div class="control">
                                    <input class="input" id="sbranch" type="text" placeholder="branch_email">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Offer</label>
                                <div class="control">
                                    <input class="input" id="courseoffer" type="text" placeholder="courseoffer">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Fees</label>
                                <div class="control">
                                    <input class="input" id="coursefee" type="text" placeholder="coursefee">
                                </div>
                                <p class="help"></p>
                            </div>

                            <div class="field">
                                <label class="label"> Course Image</label>
                                <div class="control">
                                    <input class="input" id="fileButton" type="text" placeholder="image url">
                                </div>
                                <p class="help"></p>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button id="btnSave" class="button is-success">Save changes</button>
                            <button id="btnClose" class="button">Cancel</button>
                        </footer>
                    </div>
                </div>
            </section>
            <script src="https://code.jquery.com/jquery-2.2.4.min.js"
                    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                    crossorigin="anonymous"></script>
            <script src="https://www.gstatic.com/firebasejs/4.10.0/firebase.js"></script>
            <script>
                var nextkey = 0;
                var config = {
                    apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
                    authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
                    databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
                    projectId: "laravelwithfirebase-6ae34",
                    storageBucket: "laravelwithfirebase-6ae34.appspot.com",
                    messagingSenderId: "871324337451"
                };

                firebase.initializeApp(config);
                var database = firebase.database();

                database.ref('course').on('child_added', function (data) {
                    add_data_table(data.val().courseName, data.val().fileButton, data.val().fileButton, data.val().sbranch, data.val().courseoffer, data.val().coursefee, data.key);
                    var lastkey = data.key;
                    nextkey = parseInt(lastkey) + 1;
                });
                database.ref('course').on('child_changed', function (data) {
                    update_data_table(data.val().courseName, data.val().fileButton, data.val().fileButton, data.val().sbranch, data.val().courseoffer, data.val().coursefee, data.key)
                });
                database.ref('course').on('child_removed', function (data) {
                    remove_data_table(data.key)
                });function add_data_table(courseName, fileButton, fileButton, sbranch, courseoffer, coursefee, key) {
                    $("#card-list").append('<div class="image" id="' + key + '"><div class="card"><div class="card-image col-lg-3"><figure class="image"><img src="' + fileButton + '"></figure></div><div class="card-content"><div class="media"><div class="media-content"><p class="branch">Course Name: ' + courseName + '</p><p class="branch">Select Branch: ' + sbranch + '</p> <p class="branch">Course Offer: ' + courseoffer + '</p> <p class="branch">Course Fees: ' + coursefee + '</p></div></div></div><footer class="card-footer"><a href="#" data-key="' + key + '" class="card-footer-item btnEdit">Edit</a><a href="#" class="card-footer-item btnRemove"  data-key="' + key + '">Remove</a></footer></div></div>');
                }

                function update_data_table(courseName, fileButton, fileButton, sbranch, courseoffer, coursefee, key) {
                    $("#card-list #" + key).html('<div class="card"><div class="card-image"><figure class="image"><img src="' + fileButton + '"></figure></div><div class="card-content"><div class="media"><div class="media-content"><p class="title is-4">' + courseName + '</p><p class="subtitle is-6">' + sbranch + '</p><p class="subtitle is-6">' + courseoffer + '</p><p class="subtitle is-6">' + coursefee + '</p></div></div></div><footer class="card-footer"><a href="#" class="card-footer-item btnEdit"  data-key="' + key + '">Edit</a><a  data-key="' + key + '" href="#" class="card-footer-item btnRemove">Remove</a></footer></div>');
                }

                function remove_data_table(key) {
                    $("#card-list #" + key).remove();
                }

                function new_data(courseName, sbranch, fileButton, courseoffer, coursefee, key) {
                    database.ref('course/' + key).set({
                        courseName: courseName,
                        sbranch: sbranch,
                        fileButton: fileButton,
                        courseoffer: courseoffer,
                        coursefee: coursefee
                    });
                }

                function update_data(courseName, sbranch, fileButton,courseoffer, coursefee, key) {
                    database.ref('course/' + key).update({
                        courseName: courseName,
                        sbranch: sbranch,
                        fileButton: fileButton,
                        courseoffer: courseoffer,
                        coursefee: coursefee
                    });
                }

                $("#btnAdd").click(function () {
                    $("#courseName").val("");
                    $("#sbranch").val("");
                    $("#fileButton").val("");
                    $("#courseoffer").val("");
                    $("#coursefee").val("");
                    $("#txtType").val("N");
                    $("#txtKey").val("0");
                    $("#modal").addClass("is-active");

                });
                $("#btnSave").click(function () {
                    if ($("#txtType").val() == 'N') {
                        database.ref('course').once("value").then(function (snapshot) {
                            if (snapshot.numChildren() == 0) {
                                nextkey = 1;
                            }
                            new_data($("#courseName").val(), $("#sbranch").val(), $("#fileButton").val(), $("#courseoffer").val(), $("#coursefee").val(), nextkey);
                        });
                    } else {
                        update_data($("#courseName").val(), $("#sbranch").val(), $("#fileButton").val(), $("#courseoffer").val(), $("#coursefee").val(), $("#txtKey").val());
                    }
                    $("#btnClose").click();
                });
                $(document).on("click", ".btnEdit", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('course/' + key).once("value").then(function (snapshot) {
                        $("#courseName").val(snapshot.val().courseName);
                        $("#sbranch").val(snapshot.val().sbranch);
                        $("#fileButton").val(snapshot.val().fileButton);
                        $("#courseoffer").val(snapshot.val().courseoffer);
                        $("#coursefee").val(snapshot.val().coursefee);
                        $("#txtType").val("E");
                        $("#txtKey").val(key);
                    });
                    $("#modal").addClass("is-active");
                });
                $(document).on("click", ".btnRemove", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('course/' + key).remove();
                })

                $("#btnClose,.btnClose").click(function () {
                    $("#modal").removeClass("is-active");
                });
            </script>
        </div>
    </div>
</div>

</body>
</html>



































































