<html>
<head>
    <title> welcome to guna</title>

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>

    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
    <script>
        // Initialize Firebase

        var config = {
            apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
            authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
            databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
            projectId: "laravelwithfirebase-6ae34",
            storageBucket: "laravelwithfirebase-6ae34.appspot.com",
            messagingSenderId: "871324337451"
        };


        firebase.initializeApp(config);
    </script>
</head>
<body>
<table>
    <tr>
        <th>Id: </th>
        <th><input type="text" name="id" id="user_id" /></th>
    </tr>
    <tr>
        <th>Branch Name: </th>
        <th><input type="text" name="user_name" id="user_name" /></th>
    </tr>
    <tr>
        <th>Branch Code: </th>
        <th><input type="text" name="bcode" id="bcode" /></th>
    </tr>
    <tr>
        <th>Branch Email: </th>
        <th><input type="text" name="bemail" id="bemail" /></th>
    </tr>
    <tr>
        <th>Branch Password: </th>
        <th><input type="text" name="bpassword" id="bpassword" /></th>
    </tr>
    <tr>
        <td colspan="2">
            <input type="button" value="Save" onclick="save_user();" />
            <input type="button" value="Update" onclick="update_user();" />
            <input type="button" value="Delete" onclick="delete_user();" />
        </td>
    </tr>
</table>

<h3>Users List</h3>

<table id="tbl_users_list" border="1">
    <tr>
        <td>#ID</td>
        <td>Branch NAME</td>
        <td>Branch Code</td>
        <td>Branch Email</td>
        <td>Branch Password</td>
    </tr>
</table>

<script>

    var tblUsers = document.getElementById('tbl_users_list');
    var databaseRef = firebase.database().ref('users/');
    var rowIndex = 1;

    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblUsers.insertRow(rowIndex);
            var cellId = row.insertCell(0);
            var cellName = row.insertCell(1);
            var cellCode = row.insertCell(2);
            var cellEmail = row.insertCell(3);
            var cellPassword = row.insertCell(4);
            cellId.appendChild(document.createTextNode(childKey));
            cellName.appendChild(document.createTextNode(childData.user_name));
            cellCode.appendChild(document.createTextNode(childData.bcode));
            cellEmail.appendChild(document.createTextNode(childData.bemail));
            cellPassword.appendChild(document.createTextNode(childData.bpassword));

            rowIndex = rowIndex + 1;
        });
    });

    function save_user(){
        var user_name = document.getElementById('user_name').value;
        var bcode = document.getElementById('bcode').value;
        var bemail = document.getElementById('bemail').value;
        var bpassword = document.getElementById('bpassword').value;

        var uid = firebase.database().ref().child('users').push().key;

        var data = {
            user_id: uid,
            user_name: user_name,
            bcode: bcode,
            bemail: bemail,
            bpassword: bpassword
        }

        var updates = {};
        updates['/users/' + uid] = data;
        firebase.database().ref().update(updates);

        alert('The user is created successfully!');
        reload_page();
    }

    function update_user(){
        var user_name = document.getElementById('user_name').value;
        var bcode = document.getElementById('bcode').value;
        var bemail = document.getElementById('bemail').value;
        var bpassword = document.getElementById('bpassword').value;
        var user_id = document.getElementById('user_id').value;

        var data = {
            user_id: user_id,
            user_name: user_name,
            bcode: bcode,
            bemail: bemail,
            bpassword: bpassword
        }

        var updates = {};
        updates['/users/' + user_id] = data;
        firebase.database().ref().update(updates);

        alert('The user is updated successfully!');

        reload_page();
    }

    function delete_user(){
        var user_id = document.getElementById('user_id').value;

        firebase.database().ref().child('/users/' + user_id).remove();
        alert('The user is deleted successfully!');
        reload_page();
    }

    function reload_page(){
        window.location.reload();
    }

</script>

</body>
</html>