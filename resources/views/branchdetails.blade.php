<html>
<head>
    <title> welcome to guna</title>

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>

    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
    <script>
        // Initialize Firebase

        var config = {
            apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
            authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
            databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
            projectId: "laravelwithfirebase-6ae34",
            storageBucket: "laravelwithfirebase-6ae34.appspot.com",
            messagingSenderId: "871324337451"
        };


        firebase.initializeApp(config);
    </script>
</head>
<body>



    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h3>Branch Details</h3>
        </div>
        <div class="modal-body">
            <table id="tbl_course_list" class="table" border="1">
                <tr>
                    <td>#ID</td>
                    <td>Branch NAME</td>
                    <td>Branch Code</td>
                    <td>Branch Email</td>
                    <td>Branch Password</td>
                </tr>
            </table>

            <script>

                var tblcourse = document.getElementById('tbl_course_list');
                var databaseRef = firebase.database().ref('course/');
                var rowIndex = 1;

                databaseRef.once('value', function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        var childKey = childSnapshot.key;
                        var childData = childSnapshot.val();

                        var row = tblcourse.insertRow(rowIndex);
                        var cellId = row.insertCell(0);
                        var cellName = row.insertCell(1);
                        var cellCode = row.insertCell(2);
                        var cellEmail = row.insertCell(3);
                        var cellPassword = row.insertCell(4);

                        cellId.appendChild(document.createTextNode(childKey));
                        cellName.appendChild(document.createTextNode(childData.branchName));
                        cellCode.appendChild(document.createTextNode(childData.branchCode));
                        cellEmail.appendChild(document.createTextNode(childData.branchEmail));
                        cellPassword.appendChild(document.createTextNode(childData.branchPwd));


                        rowIndex = rowIndex + 1;
                    });
                });

                function save_user(){
                    var branchName = document.getElementById('branchName').value;
                    var branchCode = document.getElementById('branchCode').value;
                    var branchEmail = document.getElementById('branchEmail').value;
                    var branchPwd = document.getElementById('branchPwd').value;
                    var branchImage = document.getElementById('branchImage').value;

                    var uid = firebase.database().ref().child('course').push().key;

                    var data = {
                        user_id: uid,
                        branchName: branchName,
                        branchCode: branchCode,
                        branchEmail: branchEmail,
                        branchPwd: branchPwd,
                        branchImage: branchImage
                    }

                    var updates = {};
                    updates['/course/' + uid] = data;
                    firebase.database().ref().update(updates);

                    alert('The user is created successfully!');
                    reload_page();
                }

                function update_user(){
                    var branchName = document.getElementById('branchName').value;
                    var branchCode = document.getElementById('branchCode').value;
                    var branchEmail = document.getElementById('branchEmail').value;
                    var branchPwd = document.getElementById('branchPwd').value;
                    var user_id = document.getElementById('user_id').value;

                    var data = {
                        user_id: user_id,
                        branchName: branchName,
                        branchCode: branchCode,
                        branchEmail: branchEmail,
                        branchPwd: branchPwd
                    }

                    var updates = {};
                    updates['/course/' + user_id] = data;
                    firebase.database().ref().update(updates);

                    alert('The user is updated successfully!');

                    reload_page();
                }

                function delete_user(){
                    var user_id = document.getElementById('user_id').value;

                    firebase.database().ref().child('/course/' + user_id).remove();
                    alert('The user is deleted successfully!');
                    reload_page();
                }

                function reload_page(){
                    window.location.reload();
                }

            </script>
            <script>
                // Get the modal
                var modal = document.getElementById('myModal');

                // Get the button that opens the modal
                var btn = document.getElementById("myBtn");

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks the button, open the modal
                btn.onclick = function() {
                    modal.style.display = "block";
                }

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                    modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            </script>
        </div>
    </div>

</div>



</body>
</html>




