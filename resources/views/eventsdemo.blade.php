<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>firebase by seegatesite.com</title>
    <link rel="stylesheet" href="{{URL::asset('css/css_sg/content.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>
</head>
<body bgcolor="#333333">
<div class="container-fluid">
    <div class="container">
        <header class="modal-card-head">
            <p class="modal-card-title">Magme Admission: Branch</p>
            <div class="back_page"><a class="back_page" href="magme_eventsdetails">Back</a> </div>
        </header>

        <div id="card-list" class="row is-mobile">
        </div>
    </div>
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <section class="section">
                <div id="modal" class="modal">
                    <div class="modal-backgrounds"></div>
                    <div class="modal-card">
                        <header class="modal-card-head">
                            <p class="modal-card-title">Magme Admission: Events</p>
                            <button class="btnClose delete" aria-label="close"></button>
                        </header>
                        <section class="modal-card-body">
                            <div class="field">
                                <label class="label">Event Name</label>
                                <div class="control">
                                    <input type="hidden" id="txtType">
                                    <input type="hidden" id="txtKey">
                                    <input class="input" id="eventName" type="text" placeholder="eventName">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Select Branch</label>
                                <div class="control">
                                    <input class="input" id="toBranch" type="text" placeholder="toBranch">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Event Date</label>
                                <div class="control">
                                    <input class="input" id="eventDate" type="text" placeholder="eventDate">
                                </div>
                                <p class="help"></p>
                            </div>
                            <div class="field">
                                <label class="label">Event Description</label>
                                <div class="control">
                                    <input class="input" id="eventDesc" type="text" placeholder="eventDesc">
                                </div>
                                <p class="help"></p>
                            </div>
                        </section>
                        <footer class="modal-card-foot">
                            <button id="btnSave" class="button is-success">Save changes</button>
                            <button id="btnClose" class="button">Cancel</button>
                        </footer>
                    </div>
                </div>
            </section>
            <script src="https://code.jquery.com/jquery-2.2.4.min.js"
                    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                    crossorigin="anonymous"></script>
            <script src="https://www.gstatic.com/firebasejs/4.10.0/firebase.js"></script>
            <script>
                var nextkey = 0;
                var config = {
                    apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
                    authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
                    databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
                    projectId: "laravelwithfirebase-6ae34",
                    storageBucket: "laravelwithfirebase-6ae34.appspot.com",
                    messagingSenderId: "871324337451"
                };

                firebase.initializeApp(config);
                var database = firebase.database();

                database.ref('Event').on('child_added', function (data) {
                    add_data_table(data.val().eventName, data.val().toBranch, data.val().eventDate, data.val().eventDesc, data.key);
                    var lastkey = data.key;
                    nextkey = parseInt(lastkey) + 1;
                });
                database.ref('Event').on('child_changed', function (data) {
                    update_data_table(data.val().eventName, data.val().toBranch, data.val().eventDate, data.val().eventDesc, data.key)
                });
                database.ref('Event').on('child_removed', function (data) {
                    remove_data_table(data.key)
                });

                var tblevents = document.getElementById('tbl_events_list');
                var databaseRef = firebase.database().ref('Event/');
                var rowIndex = 1;

                databaseRef.once('value', function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        var childKey = childSnapshot.key;
                        var childData = childSnapshot.val();

                        var row = tblevents.insertRow(rowIndex);
                        var cellId = row.insertCell(0);
                        var cellName = row.insertCell(1);
                        var cellCode = row.insertCell(2);
                        var cellEmail = row.insertCell(3);
                        var cellPassword = row.insertCell(4);

                        cellId.appendChild(document.createTextNode(childKey));
                        cellName.appendChild(document.createTextNode(childData.eventName));
                        cellCode.appendChild(document.createTextNode(childData.toBranch));
                        cellEmail.appendChild(document.createTextNode(childData.eventDate));
                        cellPassword.appendChild(document.createTextNode(childData.eventDesc));


                        rowIndex = rowIndex + 1;
                    });
                });


                function add_data_table(eventName, toBranch, eventDate, eventDesc, key) {
                    $("#card-list").append('<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" id="' + key + '"><div class="card col-md-12 col-sm-12 col-xs-12 col-lg-12 edit_delete padding_left_right_null">' +
                        '<div class="card-image"><div class="media"><div class="media-content">' +
                        '<p class="branch">Event Name:  ' + eventName + '</p>' +
                        '</div></div></div><footer class="card-footer">' +
                        '<a href="#" data-key="' + key + '" class="card-footer-item btnEdit">Edit</a>' +
                        '<a href="#" class="card-footer-item btnRemove"  data-key="' + key + '">Remove</a></footer></div></div>');
                }


                function remove_data_table(key) {
                    $("#card-list #" + key).remove();
                }

                function new_data(eventName, toBranch, eventDate, eventDesc, key) {
                    database.ref('Event/' + key).set({
                        eventName: eventName,
                        toBranch: toBranch,
                        eventDate: eventDate,
                        eventDesc: eventDesc
                    });
                }

                function update_data(eventName, toBranch,eventDate, eventDesc, key) {
                    database.ref('Event/' + key).update({
                        eventName: eventName,
                        toBranch: toBranch,
                        eventDate: eventDate,
                        eventDesc: eventDesc
                    });
                }

                $("#btnAdd").click(function () {
                    $("#eventName").val("");
                    $("#toBranch").val("");
                    $("#eventDate").val("");
                    $("#eventDesc").val("");
                    $("#txtType").val("N");
                    $("#txtKey").val("0");
                    $("#modal").addClass("is-active");

                });
                $("#btnSave").click(function () {
                    if ($("#txtType").val() == 'N') {
                        database.ref('events').once("value").then(function (snapshot) {
                            if (snapshot.numChildren() == 0) {
                                nextkey = 1;
                            }
                            new_data($("#eventName").val(), $("#toBranch").val(),  $("#eventDate").val(), $("#eventDesc").val(), nextkey);
                        });
                    } else {
                        update_data($("#eventName").val(), $("#toBranch").val(),  $("#eventDate").val(), $("#eventDesc").val(), $("#txtKey").val());
                    }
                    $("#btnClose").click();
                });
                $(document).on("click", ".btnEdit", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('Event/' + key).once("value").then(function (snapshot) {
                        $("#eventName").val(snapshot.val().eventName);
                        $("#toBranch").val(snapshot.val().toBranch);
                        $("#eventDate").val(snapshot.val().eventDate);
                        $("#eventDesc").val(snapshot.val().eventDesc);
                        $("#txtType").val("E");
                        $("#txtKey").val(key);
                    });
                    $("#modal").addClass("is-active");
                });
                $(document).on("click", ".btnRemove", function (event) {
                    event.preventDefault();
                    key = $(this).attr("data-key");
                    database.ref('Event/' + key).remove();
                })

                $("#btnClose,.btnClose").click(function () {
                    $("#modal").removeClass("is-active");
                });
            </script>
        </div>
    </div>
</div>

</body>
</html>




































































