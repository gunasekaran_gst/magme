
<!DOCTYPE html>
<html>
<head>
    <title>Admin Dashboard</title>
  <meta charset="utf-8">
  <meta content="ie=edge" http-equiv="x-ua-compatible">
  <meta content="template language" name="keywords">
  <meta content="Tamerlan Soziev" name="author">
  <meta content="Admin dashboard html template" name="description">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <link href="{{asset('favicon.png')}}" rel="shortcut icon">
  <link href="{{asset('apple-touch-icon.png')}}" rel="apple-touch-icon">
  <link href="{{asset('https://fonts.googleapis.com/css?family=Rubik:300,400,500')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('home-page/assets/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
  <link href="{{asset('home-page/assets/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
  <link href="{{asset('home-page/assets/bower_components/dropzone/dist/dropzone.css')}}" rel="stylesheet">
  <link href="{{asset('home-page/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('home-page/assets/bower_components/fullcalendar/dist/fullcalendar.min.css')}}" rel="stylesheet">
  <link href="{{asset('home-page/assets/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" rel="stylesheet">
  <link href="{{asset('home-page/assets/bower_components/slick-carousel/slick/slick.css')}}" rel="stylesheet">
  <link href="{{asset('home-page/assets/css1/main.css?version=4.4.0')}}" rel="stylesheet">

  <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
  <script src="{{URL::asset('js/bootstrap.js') }}"></script>

  <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{URL::asset('css/style.css') }}">
  <link rel="stylesheet" href="{{URL::asset('css/content.css') }}">
  <link rel="stylesheet" href="{{URL::asset('css/animate.css') }}">
  <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
  <script src="{{URL::asset('js/jquery.js') }}"></script>
  <script src="{{URL::asset('js/bootstrap.js') }}"></script>
  <script src="{{URL::asset('js/wow.js') }}"></script>
  <script src="{{URL::asset('js/custom.js') }}"></script>
  <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
  <script>
      // Initialize Firebase

      var config = {
          apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
          authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
          databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
          projectId: "laravelwithfirebase-6ae34",
          storageBucket: "laravelwithfirebase-6ae34.appspot.com",
          messagingSenderId: "871324337451"
      };


      firebase.initializeApp(config);
  </script>

</head>
<body class="menu-position-side menu-side-left full-screen color-scheme-dark">
<div class="all-wrapper solid-bg-all">
  <div class="search-with-suggestions-w">
    <div class="search-with-suggestions-modal">
      <div class="element-search">
        <input class="search-suggest-input" placeholder="Start typing to search..." type="text">
        <div class="close-search-suggestions">
          <i class="os-icon os-icon-x"></i>
        </div>

      </div>
    </div>
  </div>
  <div class="layout-w">
    <!--------------------
    START - Mobile Menu
    -------------------->
    <!--------------------
    END - Mobile Menu
    --------------------><!--------------------
        START - Main Menu
        -------------------->
    <div class="menu-w menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright menu-activated-on-hover color-scheme-dark color-style-transparent selected-menu-color-bright">
      <div class="logo-w">
        <a class="logo" href="index.html">
          <div class="logo-element"></div>
          <div class="logo-label">
            Clean Admin
          </div>
        </a>
      </div>
      <div class="menu-actions">
        <!--------------------
        START - Messages Link in secondary top menu
        -------------------->
        <div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
          <i class="os-icon os-icon-mail-14"></i>
          <div class="new-messages-count">
            12
          </div>
          <div class="os-dropdown light message-list">
            <ul>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar1.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      John Mayers
                    </h6>
                    <h6 class="message-title">
                      Account Update
                    </h6>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar2.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      Phil Jones
                    </h6>
                    <h6 class="message-title">
                      Secutiry Updates
                    </h6>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar3.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      Bekky Simpson
                    </h6>
                    <h6 class="message-title">
                      Vacation Rentals
                    </h6>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar4.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      Alice Priskon
                    </h6>
                    <h6 class="message-title">
                      Payment Confirmation
                    </h6>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <!--------------------
        END - Messages Link in secondary top menu
        --------------------><!--------------------
            START - Settings Link in secondary top menu
            -------------------->
        <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-right">
          <i class="os-icon os-icon-ui-46"></i>
          <div class="os-dropdown">
            <div class="icon-w">
              <i class="os-icon os-icon-ui-46"></i>
            </div>
            <ul>
              <li>
                <a href="users_profile_small.html"><i class="os-icon os-icon-ui-49"></i><span>Profile Settings</span></a>
              </li>
              <li>
                <a href="users_profile_small.html"><i class="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
              </li>
              <li>
                <a href="users_profile_small.html"><i class="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
              </li>
              <li>
                <a href="users_profile_small.html"><i class="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
              </li>
            </ul>
          </div>
        </div>
        <!--------------------
        END - Settings Link in secondary top menu
        --------------------><!--------------------
            START - Messages Link in secondary top menu
            -------------------->
        <div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
          <i class="os-icon os-icon-zap"></i>
          <div class="new-messages-count">
            4
          </div>
          <div class="os-dropdown light message-list">
            <div class="icon-w">
              <i class="os-icon os-icon-zap"></i>
            </div>
            <ul>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar1.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      John Mayers
                    </h6>
                    <h6 class="message-title">
                      Account Update
                    </h6>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar2.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      Phil Jones
                    </h6>
                    <h6 class="message-title">
                      Secutiry Updates
                    </h6>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar3.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      Bekky Simpson
                    </h6>
                    <h6 class="message-title">
                      Vacation Rentals
                    </h6>
                  </div>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="user-avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar4.jpg')}}">
                  </div>
                  <div class="message-content">
                    <h6 class="message-from">
                      Alice Priskon
                    </h6>
                    <h6 class="message-title">
                      Payment Confirmation
                    </h6>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <!--------------------
        END - Messages Link in secondary top menu
        -------------------->
      </div>
      <div class="element-search autosuggest-search-activator">
        <input placeholder="Start typing to search..." type="text">
      </div>
      <h1 class="menu-page-header">
        Page Header
      </h1>
      <ul class="main-menu">
        <li class="sub-header">
          <span>Layouts</span>
        </li>
        <li class="selected has-sub-menu">
          <a href="index.html">
            <div class="icon-w">
              <div class="os-icon os-icon-layout"></div>
            </div>
            <span><font color="white" size="3"> Magme Admission</font></span></a>
          <div class="sub-menu-w">
            <div class="sub-menu-header">
                <font color="white" size="3"> Magme Admission</font>
            </div>
            <div class="sub-menu-icon">
              <i class="os-icon os-icon-layout"></i>
            </div>
            <div class="sub-menu-i">
              <ul class="sub-menu">
                <li>
                  <a href="users"><font color="white" size="2">Branch Register</font></a>
                </li>
                <li>
                  <a href="course"><font color="white" size="2">Course Register</font></a>
                </li>
                <li>
                  <a href="events"><font color="white" size="2">Events Register</font></a>
                </li>
                <li>
                  <a href="student"><font color="white" size="2">Student Register</font></a>
                </li>
              </ul>
            </div>
          </div>
        </li>


      </ul>

    </div>
    <!--------------------
    END - Main Menu
    -------------------->
    <div class="content-w">
      <!--------------------
      START - Top Bar
      -------------------->
      <div class="top-bar color-scheme-transparent">
        <div class="fancy-selector-w">
          <div class="fancy-selector-current">
            <div class="fs-img shadowless">
              <img class="right" src="{{URL::asset('image/magmelogo.png') }}" width="60" height="100">
            </div>
            <div class="fs-main-info">
              <div class="fs-name">
                  <font color="white" size="3">  Magme Admission</font>
              </div>
            </div>
            <div class="fs-selector-trigger">
              <i class="os-icon os-icon-arrow-down4"></i>
            </div>
          </div>
          <div class="fancy-selector-options">
            <div class="fancy-selector-option">
              <div class="fs-img shadowless">
                <img class="right" src="{{URL::asset('image/magmelogo.png') }}" width="40" height="40">
              </div>
              <div class="fs-main-info">
                <div class="fs-name">
                    <a href="users"><font color="white" size="2">Branch Register</font></a>
                </div>
              </div>
            </div>
            <div class="fancy-selector-option active">
              <div class="fs-img shadowless">
                <img class="right" src="{{URL::asset('image/magmelogo.png') }}" width="40" height="40">
              </div>
              <div class="fs-main-info">
                <div class="fs-name">
                    <a href="course"><font color="white" size="2">Course Register</font></a>
                </div>
              </div>
            </div>
            <div class="fancy-selector-option">
              <div class="fs-img shadowless">
                <img class="right" src="{{URL::asset('image/magmelogo.png') }}" width="40" height="40">
              </div>
              <div class="fs-main-info">
                <div class="fs-name">
                    <a href="events"><font color="white" size="2">Events Register</font></a>
                </div>
              </div>
            </div>
              <div class="fancy-selector-option">
                  <div class="fs-img shadowless">
                    <img class="right" src="{{URL::asset('image/magmelogo.png') }}" width="40" height="40">
                  </div>
                  <div class="fs-main-info">
                      <div class="fs-name">
                          <a  class="stu" href="student"><font color="white" size="2">Student Register</font></a>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <!--------------------
        START - Top Menu Controls
        -------------------->
        <div class="top-menu-controls">
          <div class="element-search autosuggest-search-activator">
            <input placeholder="Start typing to search..." type="text">
          </div>
          <!--------------------
          START - Messages Link in secondary top menu
          -------------------->
          <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">
            <i class="os-icon os-icon-mail-14"></i>
            <div class="new-messages-count">
              12
            </div>
            <div class="os-dropdown light message-list">
              <ul>
                <li>
                  <a href="#">
                    <div class="user-avatar-w">
                      <img alt="" src="{{asset('home-page/assets/img1/avatar1.jpg')}}">
                    </div>
                    <div class="message-content">
                      <h6 class="message-from">
                        John Mayers
                      </h6>
                      <h6 class="message-title">
                        Account Update
                      </h6>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="user-avatar-w">
                      <img alt="" src="{{asset('home-page/assets/img1/avatar2.jpg')}}">
                    </div>
                    <div class="message-content">
                      <h6 class="message-from">
                        Phil Jones
                      </h6>
                      <h6 class="message-title">
                        Secutiry Updates
                      </h6>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="user-avatar-w">
                      <img alt="" src="{{asset('home-page/assets/img1/avatar3.jpg')}}">
                    </div>
                    <div class="message-content">
                      <h6 class="message-from">
                        Bekky Simpson
                      </h6>
                      <h6 class="message-title">
                        Vacation Rentals
                      </h6>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="user-avatar-w">
                      <img alt="" src="{{asset('home-page/assets/img1/avatar4.jpg')}}">
                    </div>
                    <div class="message-content">
                      <h6 class="message-from">
                        Alice Priskon
                      </h6>
                      <h6 class="message-title">
                        Payment Confirmation
                      </h6>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!--------------------
          END - Messages Link in secondary top menu
          --------------------><!--------------------
              START - Settings Link in secondary top menu
              -------------------->
          <div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-left">
            <i class="os-icon os-icon-ui-46"></i>
            <div class="os-dropdown">
              <div class="icon-w">
                <i class="os-icon os-icon-ui-46"></i>
              </div>
              <ul>
                <li>
                  <a href="users_profile_small.html"><i class="os-icon os-icon-ui-49"></i><span>Profile Settings</span></a>
                </li>
                <li>
                  <a href="users_profile_small.html"><i class="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
                </li>
                <li>
                  <a href="users_profile_small.html"><i class="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
                </li>
                <li>
                  <a href="users_profile_small.html"><i class="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
                </li>
              </ul>
            </div>
          </div>
          <!--------------------
          END - Settings Link in secondary top menu
          --------------------><!--------------------
              START - User avatar and menu in secondary top menu
              -------------------->
          <div class="logged-user-w">
            <div class="logged-user-i">
              <div class="avatar-w">
                <img alt="" src="{{asset('home-page/assets/img1/avatar1.jpg')}}">
              </div>
              <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                  <div class="avatar-w">
                    <img alt="" src="{{asset('home-page/assets/img1/avatar1.jpg')}}">
                  </div>
                  <div class="logged-user-info-w">
                    <div class="logged-user-name">
                      Maria Gomez
                    </div>
                    <div class="logged-user-role">
                      Administrator
                    </div>
                  </div>
                </div>
                <div class="bg-icon">
                  <i class="os-icon os-icon-wallet-loaded"></i>
                </div>
                <ul>
                  <li>
                    <a href="apps_email.html"><i class="os-icon os-icon-mail-01"></i><span>Incoming Mail</span></a>
                  </li>
                  <li>
                    <a href="users_profile_big.html"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                  </li>
                  <li>
                    <a href="users_profile_small.html"><i class="os-icon os-icon-coins-4"></i><span>Billing Details</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="os-icon os-icon-others-43"></i><span>Notifications</span></a>
                  </li>

                </ul>
              </div>
            </div>
          </div>
          <!--------------------
          END - User avatar and menu in secondary top menu
          -------------------->
        </div>
        <!--------------------
        END - Top Menu Controls
        -------------------->
      </div>
      <!--------------------
      END - Top Bar
      -------------------->


            <div class="element-wrapper">
                <h6 class="element-header">
                  Branch Details
                </h6>
            </div>

        <table id="tbl_users_list"  class="table table-padded border="1">
                            <tr>
                                <td>ID</td>
                                <td>Branch NAME</td>
                                <td>Branch Code</td>
                                <td>Branch Email</td>
                                <td>Branch Password</td>

                            </tr>
                        </table>

                        <script>

                            var tblUsers = document.getElementById('tbl_users_list');
                            var databaseRef = firebase.database().ref('users/');
                            var rowIndex = 1;

                            databaseRef.once('value', function(snapshot) {
                                snapshot.forEach(function(childSnapshot) {
                                    var childKey = childSnapshot.key;
                                    var childData = childSnapshot.val();

                                    var row = tblUsers.insertRow(rowIndex);
                                    var cellId = row.insertCell(0);
                                    var cellName = row.insertCell(1);
                                    var cellCode = row.insertCell(2);
                                    var cellEmail = row.insertCell(3);
                                    var cellPassword = row.insertCell(4);

                                    cellId.appendChild(document.createTextNode(childKey));
                                    cellName.appendChild(document.createTextNode(childData.branchName));
                                    cellCode.appendChild(document.createTextNode(childData.branchCode));
                                    cellEmail.appendChild(document.createTextNode(childData.branchEmail));
                                    cellPassword.appendChild(document.createTextNode(childData.branchPwd));


                                    rowIndex = rowIndex + 1;
                                });
                            });

                            function save_user(){
                                var branchName = document.getElementById('branchName').value;
                                var branchCode = document.getElementById('branchCode').value;
                                var branchEmail = document.getElementById('branchEmail').value;
                                var branchPwd = document.getElementById('branchPwd').value;
                                var branchImage = document.getElementById('branchImage').value;

                                var uid = firebase.database().ref().child('users').push().key;

                                var data = {
                                    user_id: uid,
                                    branchName: branchName,
                                    branchCode: branchCode,
                                    branchEmail: branchEmail,
                                    branchPwd: branchPwd,
                                    branchImage: branchImage
                                }

                                var updates = {};
                                updates['/users/' + uid] = data;
                                firebase.database().ref().update(updates);

                                alert('The user is created successfully!');
                                reload_page();
                            }

                            function update_user(){
                                var branchName = document.getElementById('branchName').value;
                                var branchCode = document.getElementById('branchCode').value;
                                var branchEmail = document.getElementById('branchEmail').value;
                                var branchPwd = document.getElementById('branchPwd').value;
                                var user_id = document.getElementById('user_id').value;

                                var data = {
                                    user_id: user_id,
                                    branchName: branchName,
                                    branchCode: branchCode,
                                    branchEmail: branchEmail,
                                    branchPwd: branchPwd
                                }

                                var updates = {};
                                updates['/users/' + user_id] = data;
                                firebase.database().ref().update(updates);

                                alert('The user is updated successfully!');

                                reload_page();
                            }

                            function delete_user(){
                                var user_id = document.getElementById('user_id').value;

                                firebase.database().ref().child('/users/' + user_id).remove();
                                alert('The user is deleted successfully!');
                                reload_page();
                            }

                            function reload_page(){
                                window.location.reload();
                            }
                        </script>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_80">
        </div>














          <!--END - Recent Ticket Comments-->
        </div>
      </div><!--------------------
              START - Color Scheme Toggler
              -------------------->
      <!-- <div class="floated-colors-btn second-floated-btn">
        <div class="os-toggler-w on">
          <div class="os-toggler-i">
            <div class="os-toggler-pill"></div>
          </div>
        </div>
        <span>Dark </span><span>Colors</span>
      </div> -->
      <!--------------------
      END - Color Scheme Toggler
      --------------------><!--------------------
              START - Demo Customizer
              -------------------->

      <!--------------------
      END - Demo Customizer
      --------------------><!--------------------
              START - Chat Popup Box
              -------------------->

      <!--------------------
      END - Chat Popup Box
      -------------------->
    </div>
  </div>
</div>
</div>
<div class="display-type"></div>
</div>
<script src="{{asset('home-page/assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/moment/moment.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap-validator/dist/validator.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/dropzone/dist/dropzone.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/editable-table/mindmup-editabletable.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/slick-carousel/slick/slick.min.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/util.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/alert.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/button.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/carousel.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/collapse.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/dropdown.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/modal.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/tab.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/tooltip.js')}}"></script>
<script src="{{asset('home-page/assets/bower_components/bootstrap/js/dist/popover.js')}}"></script>
<script src="{{asset('home-page/assets/js1/demo_customizer.js?version=4.4.0')}}"></script>
<script src="{{asset('home-page/assets/js1/main.js?version=4.4.0')}}"></script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-XXXXXXX-9', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>


