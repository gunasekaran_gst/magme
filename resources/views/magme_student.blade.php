<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:21 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Mosaddek" />
    <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="javascript:;" type="image/png">

    <title>Magme - Responsive Admin Dashboard Template</title>

    <!--easy pie chart-->
    <link href="js/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />

    <!--vector maps -->
    <link rel="stylesheet" href="js/vector-map/jquery-jvectormap-1.1.1.css">

    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">

    <!--switchery-->
    <link href="js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />

    <!--jquery-ui-->
    <link href="js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />

    <!--iCheck-->
    <link href="js/icheck/skins/all.css" rel="stylesheet">

    <link href="css/owl.carousel.css" rel="stylesheet">


    <!--common style-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>


    <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- sidebar left start-->
    <div class="sidebar-left">
        <!--responsive view logo start-->
        <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
            <a href="magme_student">
                <!--<i class="fa fa-maxcdn"></i>-->
                <span class="brand-name">Magme</span>
            </a>
        </div>
        <!--responsive view logo end-->

        <div class="sidebar-left-info">
            <!-- visible small devices start-->
            <div class=" search-field">  </div>
            <!-- visible small devices end-->

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked side-Navigation">
                <li>
                    <h3 class="Navigation-title">Created Of the Leader</h3>
                </li>
                <li class="active"><a href=""><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Register</span></a>
                    <ul class="child-list">
                        <li><a href="index">Branch Register</a></li>
                        <li><a href="magme_course">Course Register</a></li>
                        <li><a href="magme_events">Events Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>View Details</span></a>
                    <ul class="child-list">
                        <li><a href="magme_branchdetails">Branch Register</a></li>
                        <li><a href="magme_coursedetails">Course Register</a></li>
                        <li><a href="magme_eventsdetails">Events Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Magme Edit/Delete</span></a>
                    <ul class="child-list">
                        <li><a href="demo">Branch Register</a></li>
                        <li><a href="coursedemo">Course Register</a></li>
                        <li><a href="eventsdemo">Events Register</a></li>
                    </ul>
                </li>

            </ul>
            <!--sidebar nav end-->

            <!--sidebar widget start-->

            <!--sidebar widget end-->

        </div>
    </div>
    <!-- sidebar left end-->

    <script>
        function validateForm() {
            var x = document.forms["myForm"]["fname"].value;
            if (x == "") {
                alert("Name must be filled out");
                return false;
            }
        }
    </script>

    <!-- body content start-->
    <div class="body-content" >
        <div class="col-lg-8 padd_top_10">
            <section class="panel">
                <header class="panel-heading">
                    Student
                </header>
                <div class="panel-body">
                    <form name="myForm"
                          onsubmit="return validateForm()" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Aadhar No</label>&nbsp;&nbsp;<i class="fa fa-spinner fa-spin"></i>
                        <input type="text"  required class="form-control" name="aadharNo" id="aadharNo" placeholder="aadharNo">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Student Name</label>
                        <input type="text"required  class="form-control"  name="stuName" id="stuName" placeholder="stuName">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Student Mobile</label>
                        <input type="text"  required class="form-control" name="stuMob" id="stuMob"placeholder="stuMob">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Student Email</label>
                        <input type="text" required class="form-control"  name="stuEmail" id="stuEmail" placeholder="stuEmail">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Student Father Name</label>
                        <input type="text"  required class="form-control" name="stuFather" id="stuFather" placeholder="stuFather">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Register Date</label>
                        <input type="date" required class="form-control"  name="regDate" id="regDate" placeholder="regDate">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">txnId</label>
                        <input type="text"  required class="form-control" name="txnId" id="txnId" placeholder="txnId">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">paid Amount</label>
                        <input type="text" required class="form-control"  name="paidAmt" id="paidAmt"placeholder="paidAmt">

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">First Pay</label>
                        <input type="text" required class="form-control"  name="firstPay" id="firstPay" placeholder="firstPay">

                    </div>
                    </form>

                    <div class="form-group">
                        <input type="buttons"  class="save_buttons" value="Save" onclick="getAllCourse();"/>
                        <a href="magme_student"> <input type="buttons"  class="save_buttons" value="Cancle"/> </a>
                    </div>

                </div>
            </section>
        </div>
        <!-- header section start-->
        <div class="header-section">

            <!--logo and logo icon start-->
            <div class="logo dark-logo-bg hidden-xs hidden-sm">
                <a href="magme_student">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">Magme</span>
                </a>
            </div>

            <!--logo and logo icon end-->

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
            <!--toggle button end-->

            <!--mega menu start-->
            <div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
                <ul class="nav navbar-nav">
                    <!-- Classic list -->
                    <li class="dropdown"><a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle">Magme Admission &nbsp;  <b
                                    class=" fa fa-angle-down"></b></a>
                        <ul class="dropdown-menu wide-full">
                            <li>
                                <!-- Content container to add padding -->
                                <div class="yamm-content">
                                    <div class="row">
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Register</p>
                                            </li>
                                            <li><a href="index"> Branch Register</a>
                                            </li>
                                            <li><a href="magme_course"> Course Register</a>
                                            </li>
                                            <li><a href="magme_events"> Events Register </a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Details</p>
                                            </li>
                                            <li><a href="magme_branchdetails"> Branch Details</a>
                                            </li>
                                            <li><a href="magme_coursedetails"> Course Details</a>
                                            </li>
                                            <li><a href="magme_eventsdetails"> Events Details </a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Edit/Delete</p>
                                            </li>
                                            <li><a href="demo"> Branch Edit/Delete</a>
                                            </li>
                                            <li><a href="coursedemo"> Course Edit/Delete</a>
                                            </li>
                                            <li><a href="eventsdemo"> Events Edit/Delete </a>
                                            </li>
                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title"> Datewise view</p>
                                            </li>
                                            <li><a href="magme_dateview">Student dateview</a>
                                            </li>
                                            <br>
                                            <li><a href="magme_studentdetails">Admission Details Download</a>
                                            </li>
                                            <br>
                                            <li><a href="magmelogin_studetails">Student Details Download</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <!-- Classic dropdown -->

                </ul>
            </div>
            <!--mega menu end-->


            <!--right notification start-->
            <div class="right-notification">
                <ul class="notification-menu">
                    <li>
                        <form class="search-content" action="http://thevectorlab.net/slicklab/index.html" method="post">
                            <input type="text" class="form-control" name="keyword" placeholder="Search...">
                        </form>
                    </li>

                    <li>
                        <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="img/avatar-mini.jpg" alt="">Log Out
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                            <li><a href="javascript:;">  Profile</a></li>
                            <li>
                                <a href="javascript:;">
                                    <span class="badge bg-danger pull-right">40%</span>
                                    <span>Settings</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label bg-info pull-right">new</span>
                                    Help
                                </a>
                            </li>
                            <li><a href="logins"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <li>
                        <div class="sb-toggle-right">
                            <i class="fa fa-indent"></i>
                        </div>
                    </li>

                </ul>
            </div>
            <!--right notification end-->
        </div>
            <!--mega menu end-->


            <!--right notification start-->

            <!--right notification end-->
        </div>

    </div>



    <!-- header section end-->

    <!-- page head start-->

    <!-- page head end-->

    <!--body wrapper start-->

    <!--body wrapper end-->


    <!--footer section start-->

    <!--footer section end-->


    <!-- Right Slidebar start -->
    <div class="sb-slidebar sb-right sb-style-overlay">
        <div class="right-bar">

            <span class="r-close-btn sb-close"><i class="fa fa-times"></i></span>

            <ul class="nav nav-tabs nav-justified-">
                <li class="">
                    <a href="#settings" data-toggle="tab">Magme Admission</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active " id="chat">
                    <ul>
                        <li>
                            <a href="" data-toggle="tab">Branch Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Course Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Events Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Student Register</a>
                        </li>
                    </ul>


                </div>


            </div>
        </div>
    </div>

</section>



<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>

<!--jquery-ui-->
<script src="js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>

<script src="js/jquery-migrate.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>

<!--Nice Scroll-->
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

<!--right slidebar-->
<script src="js/slidebars.min.js"></script>

<!--switchery-->
<script src="js/switchery/switchery.min.js"></script>
<script src="js/switchery/switchery-init.js"></script>

<!--flot chart -->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/flot-spline.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.pie.js"></script>

<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.crosshair.js"></script>


<!--earning chart init-->
<script src="js/earning-chart-init.js"></script>


<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--easy pie chart-->
<script src="js/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="js/easy-pie-chart.js"></script>


<!--vectormap-->
<script src="js/vector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/vector-map/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/dashboard-vmap-init.js"></script>

<!--Icheck-->
<script src="js/icheck/skins/icheck.min.js"></script>
<script src="js/todo-init.js"></script>

<!--jquery countTo-->
<script src="js/jquery-countTo/jquery.countTo.js"  type="text/javascript"></script>

<!--owl carousel-->
<script src="js/owl.carousel.js"></script>

<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>


<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script>
    // Initialize Firebase

    var config = {
        apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
        authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
        databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
        projectId: "laravelwithfirebase-6ae34",
        storageBucket: "laravelwithfirebase-6ae34.appspot.com",
        messagingSenderId: "871324337451"
    };


    firebase.initializeApp(config);
</script>
<script>

    var tblstudent = document.getElementById('tbl_student_list');
    var databaseRef = firebase.database().ref('student/');
    var rowIndex = 1;

    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblstudent.insertRow(rowIndex);
            var cellId = row.insertCell(0);
            var cellaadharNo = row.insertCell(1);
            var cellstuName = row.insertCell(2);
            var cellstuMob = row.insertCell(3);
            var cellstuEmail = row.insertCell(4);
            var cellstuFather = row.insertCell(5);
            var cellregDate = row.insertCell(6);
            var celltxnId = row.insertCell(7);
            var cellpaidAmt = row.insertCell(8);
            var cellfirstPay = row.insertCell(9);

            cellId.appendChild(document.createTextNode(childKey));
            cellaadharNo.appendChild(document.createTextNode(childData.aadharNo));
            cellstuName.appendChild(document.createTextNode(childData.stuName));
            cellstuMob.appendChild(document.createTextNode(childData.stuMob));
            cellstuEmail.appendChild(document.createTextNode(childData.stuEmail));
            cellstuFather.appendChild(document.createTextNode(childData.stuFather));
            cellregDate.appendChild(document.createTextNode(childData.regDate));
            celltxnId.appendChild(document.createTextNode(childData.txnId));
            cellpaidAmt.appendChild(document.createTextNode(childData.paidAmt));
            cellfirstPay.appendChild(document.createTextNode(childData.firstPay));

            rowIndex = rowIndex + 1;
        });
    });

    function save_user(lastid){

        var inc = lastid;
        var aadharNo = document.getElementById('aadharNo').value;
        var stuName = document.getElementById('stuName').value;
        var stuMob = document.getElementById('stuMob').value;
        var stuEmail = document.getElementById('stuEmail').value;
        var stuFather = document.getElementById('stuFather').value;
        var regDate = document.getElementById('regDate').value;
        var txnId = document.getElementById('txnId').value;
        var paidAmt = document.getElementById('paidAmt').value;
        var firstPay = document.getElementById('firstPay').value;

        if(aadharNo == '' || stuName == '' || stuMob == '' || stuEmail == '' || stuFather == '' || regDate == '' || txnId== '' || paidAmt == '' || firstPay == '') {
            alert('Please enter misfiled');

        }else {

            var uid = "1234"
            var data = {
                inc: inc,
                aadharNo: aadharNo,
                stuName: stuName,
                stuMob: stuMob,
                stuEmail: stuEmail,
                stuFather: stuFather,
                regDate: regDate,
                txnId: txnId,
                paidAmt: paidAmt,
                firstPay: firstPay
            }

            var updates = {};
            updates['/student/' + inc] = data;
            firebase.database().ref().update(updates);

            alert('The user is created successfully!');
            reload_page();
        }
    }

    function getAllCourse() {

        // console.log("getcourse");
        var person = [];
        var id = 0;
        console.log(JSON.stringify(person));

        databaseRef.once('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();

                person.push(childKey);

            });
            person.sort();
            console.log(JSON.stringify(person));
            if (person.length>0)
            {
                id =person[person.length-1];
                id++;
            }
            id = pad(id,4);
            save_user(id);
        });
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    function reload_page(){
        window.location.reload();
    }

</script>

{{--<footer>--}}
    {{--2018&copy; Log Out Technologies.--}}
    {{--</footer>--}}
</body>

<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:59 GMT -->
</html>
