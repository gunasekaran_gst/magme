<!DOCTYPE html>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
<table style="width:100%">
    <tr>
        <th>Address</th>
        <th>Tax Invoice</th>
        <th colspan="2">(Original Copy)</th>
    </tr>
    <tr>
        <th rowspan="3">Dhanyasree Precision Pvt Ltd., <br>
            C-10,Sidco Industrial Estate,Phase-1 <br>
            Sipcot, Hosur - 635 109 <br>
            GSTIN/UIN : 33AACCD1471F1Z5 <br>
            State Name : Tamil Nadu <br>
            E-Mail : dhanyasreeacs@rediffmail.com:</th>
        <td colspan="2">Invoice No:&nbsp;&nbsp;e-way Bill NoINV/004260/18-19</td>
        <td>Invoice Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br>
            <br>
            01/10/2018</td>
    </tr>
    <tr>
        <td colspan="2">Delivery Note</td>
        <td>Mode/terms of Payment</td>
    </tr>
    <tr>
        <td colspan="2">Supplier's Ref</td>
        <td>Other Reference(s)</td>
    </tr>

    <tr>
        <th rowspan="3">Consignee<br>
            Neel Auto Private Limited-C<br>
            Plot no.1,2,5 & 6, TVS Industrial Estate,<br>
            Harita-635109, Hosur:<br>
            33AABCT9860D1ZD: 33AABCT9860D1ZD</th>
        <td colspan="2">Buyer's Order No: 6500013132</td>
        <td>Date: 30/12/2016</td>
    </tr>
    <tr>
        <td colspan="2">Despatched Document No</td>
        <td>Delivery Note Date</td>
    </tr>
    <tr>
        <td colspan="2">Despatched through: TN70F1899</td>
        <td>Destination</td>
    </tr>
    <tr>
        <th rowspan="2">Buyer (if other than Consignee)<br>
            Neel Auto Private Limited-C<br>
            Plot no.1,2,5 & 6, TVS Industrial Estate,<br>
            Harita-635109, Hosur:<br>
            GSTIN/UIN:  33AABCT9860D1ZD</th>
        <td colspan="2">Bill of Lading/LR-RR No.</td>
        <td>Motor Vehicle No</td>
    </tr>
    <tr>
        <th>Terms of Delivery</th>
    </tr>
    <tr>

    </tr>

</table>
<table style="width:100%">
    <tr>
        <th width="15%">Firstname</th>
        <th width="30%">Lastname</th>
        <th width="20%">Age</th>
        <th width="20">Firstname</th>
        <th width="20">Lastname</th>
        <th width="20">wel</th>
        <th width="8%">Age</th>
    </tr>
    <tr>
        <td style="border-bottom-color: white";>Jill</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>

    </tr>
    <tr>
        <td style="border-bottom-color: white";>Jill</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td>50</td>

    </tr>
    <tr>
        <td style="border-bottom-color: white";>Jill</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td style="border-bottom-color: white";>50</td>
        <td>100</td>

    </tr>
    <tr>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white; padding-left: 150px";>&nbsp;&nbsp;&nbsp;&nbsp;CGST %&nbsp;&nbsp; 14.0</td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";>50</td>
    </tr>
    <tr>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white; padding-left: 150px";>&nbsp;&nbsp;&nbsp;&nbsp;CGST % &nbsp;&nbsp; 14.0</td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";></td>
        <td style="border-bottom-color: white";>50</td>
    </tr>

    <tr>
        <td style="border-top-color: white";></td>
        <td style="border-top-color: white; padding-left: 150px";>&nbsp;&nbsp;&nbsp;&nbsp;CGST % &nbsp;&nbsp;0.0</td>
        <td style="border-top-color: white";></td>
        <td style="border-top-color: white";></td>
        <td style="border-top-color: white";></td>
        <td style="border-top-color: white";></td>
        <td style="border-top-color: white";>50</td>

    </tr>
</table>
<table style="width:100%">
    <tr>
        <th  style="padding-left: 88%";>Total:  566565</th>

    </tr>
</table>
<table style="width:100%">
    <tr>
        <th>Seventy Five Thousand One Hundred Forty Nine rupees Twelve paise only<br>
        Seventy Five Thousand One Hundred <br>
       Seventy Five Thousand One Hundred Forty Nine paise only</th>
    </tr>
</table>
<table style="width:100%">

    <tr>
        <th style="border-bottom-color: white";>HSN/SAC</th>
        <th style="border-bottom-color: white";>Taxable value</th>
        <th colspan="2" style="border-top-color: white";>Central Tax</th>
        <th colspan="2" style="border-top-color: white";>State Tax</th>
        <th colspan="2" style="border-top-color: white";>State Tax</th>
        <th style="border-top-color: white";> Total</th>

    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>Rate</td>
        <td>Amount</td>
        <td>Rate</td>
        <td>Amount</td>
        <td>Rate</td>
        <td>Amount</td>
        <td></td>
    </tr>
    <tr>
        <td>Jill</td>
        <td>Smith</td>
        <td>50</td>
        <td>Jill</td>
        <td>Smith</td>
        <td>50</td>
        <td>Jill</td>
        <td>Smith</td>
        <td>50</td>
    </tr>
    <tr>
        <td>Jill</td>
        <td>Smith</td>
        <td>50</td>
        <td>Jill</td>
        <td>Smith</td>
        <td>50</td>
        <td>Jill</td>
        <td>Smith</td>
        <td>50</td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th>Seventy Five Thousand One Hundred Forty Nine rupees Twelve paise only<br>
            <br>
        PAN No : AACCD1471F<br>
            <br>
        Declaration<br>
            <br>
        We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct</th>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <th>
            <br>
            Customer's Seal and Signature
            <br>
        </th>
        <th>
            <br>
            For Dhanyasree Precision Private Limited
            <br>
            <br>
            Authorised Signatory
        </th>
    </tr>
</table>
        </div>
        </div>
    </div>
</div>

</body>
</html>
