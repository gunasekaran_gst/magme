<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content='@yield('keywords')'>
    <meta name="description" content='@yield('description')'>
    <title>Kabs Animation - @yield('title')</title>
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/styless.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
    <link rel="stylesheet" href="css/style.css">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>
</head>
<body>
<div id="mySidenav" class="sidenav">
    <a href="#" class="closebtn">&times;</a>
    <a href="{{ url('/home') }}">Access All Areas</a>
    <a href="{{ url('/after_effects') }}">Courses</a>
    <a href="{{ url('/certification') }}">Certification</a>
    <a href="{{ url('/bespoke') }}">Bespoke</a>
    <a href="{{ url('/enquri') }}">Enquire now</a>
    <hr>
</div>


<div class="container-fluid header">
    <div class="container-fluid menu">
        <nav class="navbar-inverse custom_class" data-spy="affix" data-offset-top="150">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle side_menu_toggle" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo col-lg-12 col-md-12 col-sm-12 col-xs-9">
                        <a href="{{ url('/') }}"> <img src="{{URL::asset('image/mag_learn_2.png') }}" width="300px" height="55px"> </a>
                    </div>
                </div>

                <div class="meanu">
                    <div class="collapse navbar-collapse topmenu" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li><a  href="{{ url('/certification') }}">Admission</a></li>
                            <li>
                                <a href="logins">
                                    <span class="slide_menu" style="font-size:17px; cursor:pointer">Log In</span>
                                </a>
                            </li>
                            <li><a  href="{{ url('/bespoke') }}">Register</a></li>


                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
@yield('content')

</body>
</html>
