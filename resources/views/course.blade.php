<html>
<head>
    <title> welcome to guna</title>

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>

    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
    <script>
        // Initialize Firebase

        var config = {
            apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
            authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
            databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
            projectId: "laravelwithfirebase-6ae34",
            storageBucket: "laravelwithfirebase-6ae34.appspot.com",
            messagingSenderId: "871324337451"
        };


        firebase.initializeApp(config);
    </script>
</head>
<body bgcolor="black">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<button id="myBtns">Branch</button>
<div id="myModal" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h3> Magme Admission: Branch </h3>
        </div>
        <div class="modal-body">
            <label for="fname">Course Id</label>
            <input type="text" name="id" id="user_id" required/>
            <label for="lname">Course Name</label>
            <input type="text" name="courseName" id="courseName" required/>
            <label for="lname">Course Duration</label>
            <input type="text" name="courseduration" id="courseduration" required/>
            <label for="lname">BranchEmail</label>
            <input type="text" name="branchEmail" id="branchEmail" required/>
            <label for="lname">Course Offer</label>
            <input type="text" name="courseoffer" id="courseoffer" required/>
            <label for="lname">Course Fee</label>
            <input type="text" name="coursefee" id="coursefee" required/>
            <label for="lname">Image Url</label>
            <input type="text" name="branchImage" id="branchImage" required/>
            <input type="button" class="button_submit" value="Save" onclick="save_user();" />
        </div>
    </div>

</div>

    <script>

        var tblcourse = document.getElementById('tbl_course_list');
        var databaseRef = firebase.database().ref('course/');
        var rowIndex = 1;

        databaseRef.once('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();

                var row = tblcourse.insertRow(rowIndex);
                var cellId = row.insertCell(0);
                var cellName = row.insertCell(1);
                var cellcourseduration = row.insertCell(2);
                var cellEmail = row.insertCell(3);
                var cellcourseoffer = row.insertCell(4);
                var cellcoursefee = row.insertCell(5);
                var cellbranchImage = row.insertCell(6);

                cellId.appendChild(document.createTextNode(childKey));
                cellName.appendChild(document.createTextNode(childData.courseName));
                cellcourseduration.appendChild(document.createTextNode(childData.courseduration));
                cellEmail.appendChild(document.createTextNode(childData.branchEmail));
                cellcourseoffer.appendChild(document.createTextNode(childData.courseoffer));
                cellcoursefee.appendChild(document.createTextNode(childData.coursefee));
                cellbranchImage.appendChild(document.createTextNode(childData.branchImage));


                rowIndex = rowIndex + 1;
            });
        });

        function save_user(){
            var courseName = document.getElementById('courseName').value;
            var courseduration = document.getElementById('courseduration').value;
            var branchEmail = document.getElementById('branchEmail').value;
            var courseoffer = document.getElementById('courseoffer').value;
            var coursefee = document.getElementById('coursefee').value;
            var branchImage = document.getElementById('branchImage').value;

            var uid = "1234";

            var data = {
                courseName: courseName,
                courseduration: courseduration,
                branchEmail: branchEmail,
                courseoffer: courseoffer,
                coursefee: coursefee,
                branchImage: branchImage
            }

            var updates = {};
            updates['/course/' + courseName] = data;
            firebase.database().ref().update(updates);

            alert('The user is created successfully!');
            reload_page();
        }

        function update_user(){
            var branchName = document.getElementById('branchName').value;
            var branchCode = document.getElementById('branchCode').value;
            var branchEmail = document.getElementById('branchEmail').value;
            var branchPwd = document.getElementById('branchPwd').value;
            var user_id = document.getElementById('user_id').value;

            var data = {
                user_id: user_id,
                branchName: branchName,
                branchCode: branchCode,
                branchEmail: branchEmail,
                branchPwd: branchPwd
            }

            var updates = {};
            updates['/course/' + user_id] = data;
            firebase.database().ref().update(updates);

            alert('The user is updated successfully!');

            reload_page();
        }

        function delete_user(){
            var user_id = document.getElementById('user_id').value;

            firebase.database().ref().child('/course/' + user_id).remove();
            alert('The user is deleted successfully!');
            reload_page();
        }

        function reload_page(){
            window.location.reload();
        }

    </script>





</div>
</body>
</html>

