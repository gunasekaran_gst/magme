<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:21 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Mosaddek" />
    <meta name="keyword" content="slick, flat, dashboard, bootstrap, admin, template, theme, responsive, fluid, retina" />
    <meta name="description" content="" />
    <link rel="shortcut icon" href="javascript:;" type="image/png">

    <title>Magme - Responsive Admin Dashboard Template</title>

    <!--easy pie chart-->
    <link href="js/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />

    <!--vector maps -->
    <link rel="stylesheet" href="js/vector-map/jquery-jvectormap-1.1.1.css">

    <!--right slidebar-->
    <link href="css/slidebars.css" rel="stylesheet">

    <!--switchery-->
    <link href="js/switchery/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />

    <!--jquery-ui-->
    <link href="js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />

    <!--iCheck-->
    <link href="js/icheck/skins/all.css" rel="stylesheet">

    <link href="css/owl.carousel.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


    <!--common style-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
    <script>
        // Initialize Firebase

        var config = {
            apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
            authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
            databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
            projectId: "laravelwithfirebase-6ae34",
            storageBucket: "laravelwithfirebase-6ae34.appspot.com",
            messagingSenderId: "871324337451"
        };


        firebase.initializeApp(config);
    </script>

</head>

<body class="sticky-header">

<section>
    <!-- sidebar left start-->
    <div class="sidebar-left">
        <!--responsive view logo start-->
        <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
            <a href="magme_branchdetails">
                <!--<i class="fa fa-maxcdn"></i>-->
                <span class="brand-name">Magme</span>
            </a>
        </div>
        <!--responsive view logo end-->

        <div class="sidebar-left-info">
            <!-- visible small devices start-->
            <div class=" search-field">  </div>
            <!-- visible small devices end-->

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked side-Navigation">
                <li>
                    <h3 class="Navigation-title">Created Of the Leader</h3>
                </li>
                <li class="active"><a href=""><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Register</span></a>
                    <ul class="child-list">
                        <li><a href="#">Branch Register</a></li>
                        <li><a href="magme_cousers">Cousers Register</a></li>
                        <li><a href="magme_events">Events Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>View Details</span></a>
                    <ul class="child-list">
                        <li><a href="magme_branchdetails">Branch Register</a></li>
                        <li><a href="magme_coursedetails">course Register</a></li>
                        <li><a href="magme_eventsdetails">Events Register</a></li>
                    </ul>
                </li>

                <li class="menu-list">
                    <a href="#"><i class="fa fa-laptop"></i>  <span>Magme Edit/Delete</span></a>
                    <ul class="child-list">
                        <li><a href="demo">Branch </a></li>
                        <br>
                        <li><a href="coursedemo">course </a></li>
                        <br>
                        <li><a href="eventsdemo">Events </a></li>
                    </ul>

                </li>

            </ul>
            <!--sidebar nav end-->

            <!--sidebar widget start-->

            <!--sidebar widget end-->

        </div>
    </div>
    <!-- sidebar left end-->

    <!-- body content start-->
    <div class="body-content" >
        <script>
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });
        </script>


        <div class="col-lg-12 padd_top_30">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading head-border">
                            Branch Details
                        </header>
                        <br>
                        <div class="date_search">
                            <input class="form-control" id="myInput" type="text" placeholder="Enter the branch name Search........">
                        </div>
                        <br>

                        <div class="table-responsive">
                            <table id="tbl_users_list" class="table" border="1">
                                <thead>
                                <tr>
                                    <td>Branch Code</td>
                                    <td>Branch NAME</td>
                                    <td>Branch Email</td>
                                    <td>Branch Password</td>
                                </tr>
                                </thead>
                                <tbody id="myTable">
                                <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                </tr>
                                </tbody>
                            </table>


                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- header section start-->
        <div class="header-section">



            <!--logo and logo icon start-->
            <div class="logo dark-logo-bg hidden-xs hidden-sm">
                <a href="#">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">Magme</span>
                </a>
            </div>

            <!--logo and logo icon end-->

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
            <!--toggle button end-->

            <!--mega menu start-->
            <div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
                <ul class="nav navbar-nav">
                    <!-- Classic list -->
                    <li class="dropdown"><a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle">Magme Admission &nbsp;  <b
                                    class=" fa fa-angle-down"></b></a>
                        <ul class="dropdown-menu wide-full">
                            <li>
                                <!-- Content container to add padding -->
                                <div class="yamm-content">
                                    <div class="row">
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Register</p>
                                            </li>
                                            <li><a href="index"> Branch Register</a>
                                            </li>
                                            <li><a href="magme_course"> Course Register</a>
                                            </li>
                                            <li><a href="magme_events"> Events Register </a>
                                            </li>

                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Details</p>
                                            </li>
                                            <li><a href="magme_branchdetails"> Branch Details</a>
                                            </li>
                                            <li><a href="magme_coursedetails"> course Details</a>
                                            </li>
                                            <li><a href="magme_eventsdetails"> Events Details </a>
                                            </li>

                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title">Edit/Delete</p>
                                            </li>
                                            <li><a href="demo"> Branch</a>
                                            </li>
                                            <br>
                                            <li><a href="coursedemo"> course </a>
                                            </li>
                                            <br>
                                            <li><a href="eventsdemo"> Events  </a>
                                            </li>

                                        </ul>
                                        <ul class="col-sm-2 list-unstyled">
                                            <li>
                                                <p class="title"> Datewise view</p>
                                            </li>
                                            <li><a href="magme_dateview">Student dateview</a>
                                            </li>
                                            <br>
                                            <li><a href="magme_studentdetails">Admission Details Download</a>
                                            </li>
                                            <br>
                                            <li><a href="magmelogin_studetails">Student Details Download</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <!-- Classic dropdown -->

                </ul>
            </div>
            <!--mega menu end-->


            <!--right notification start-->
            <div class="right-notification">
                <ul class="notification-menu">
                    <li>
                        <form class="search-content" action="http://thevectorlab.net/slicklab/index.html" method="post">
                            <input type="text" class="form-control" name="keyword" placeholder="Search...">
                        </form>
                    </li>

                    <li>
                        <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="" alt="">Log out
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                            <li><a href="logins"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <li>
                        <div class="sb-toggle-right">
                            <i class="fa fa-indent"></i>
                        </div>
                    </li>

                </ul>
            </div>
            <!--right notification end-->
        </div>

    </div>



    <!-- header section end-->

    <!-- page head start-->

    <!-- page head end-->

    <!--body wrapper start-->

    <!--body wrapper end-->


    <!--footer section start-->

    <!--footer section end-->


    <!-- Right Slidebar start -->
    <div class="sb-slidebar sb-right sb-style-overlay">
        <div class="right-bar">

            <span class="r-close-btn sb-close"><i class="fa fa-times"></i></span>

            <ul class="nav nav-tabs nav-justified-">
                <li class="">
                    <a href="#settings" data-toggle="tab">Magme Admission</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active " id="chat">
                    <ul>
                        <li>
                            <a href="" data-toggle="tab">Branch Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">users Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Events Register</a>
                        </li>
                        <li>
                            <a href="" data-toggle="tab">Student Register</a>
                        </li>
                    </ul>


                </div>


            </div>
        </div>
    </div>

</section>
<script>

    var tblusers = document.getElementById('tbl_users_list');
    var databaseRef = firebase.database().ref('Branch/');
    var rowIndex = 1;

    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblusers.insertRow(rowIndex);
            var cellId = row.insertCell(0);
            var cellName = row.insertCell(1);
            var cellEmail = row.insertCell(2);
            var cellPassword = row.insertCell(3);

            cellId.appendChild(document.createTextNode(childData.branchCode));
            cellName.appendChild(document.createTextNode(childData.branchName));
            cellEmail.appendChild(document.createTextNode(childData.branchEmail));
            cellPassword.appendChild(document.createTextNode(childData.branchPwd));


            rowIndex = rowIndex + 1;
        });
    });

    function save_user(){
        var branchName = document.getElementById('branchName').value;
        var branchCode = document.getElementById('branchCode').value;
        var branchEmail = document.getElementById('branchEmail').value;
        var branchPwd = document.getElementById('branchPwd').value;
        var branchImage = document.getElementById('branchImage').value;

        var uid = firebase.database().ref().child('Branch').push().key;

        var data = {
            user_id: uid,
            branchName: branchName,
            branchCode: branchCode,
            branchEmail: branchEmail,
            branchPwd: branchPwd,
            branchImage: branchImage
        }

        var updates = {};
        updates['/branch/' + uid] = data;
        firebase.database().ref().update(updates);

        alert('The user is created successfully!');
        reload_page();
    }

    function update_user(){
        var branchName = document.getElementById('branchName').value;
        var branchCode = document.getElementById('branchCode').value;
        var branchEmail = document.getElementById('branchEmail').value;
        var branchPwd = document.getElementById('branchPwd').value;
        var user_id = document.getElementById('user_id').value;

        var data = {
            user_id: user_id,
            branchName: branchName,
            branchCode: branchCode,
            branchEmail: branchEmail,
            branchPwd: branchPwd
        }

        var updates = {};
        updates['/users/' + user_id] = data;
        firebase.database().ref().update(updates);

        alert('The user is updated successfully!');

        reload_page();
    }

    function delete_user(){
        var user_id = document.getElementById('user_id').value;

        firebase.database().ref().child('/users/' + user_id).remove();
        alert('The user is deleted successfully!');
        reload_page();
    }

    function reload_page(){
        window.location.reload();
    }

</script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
<link rel="Stylesheet" src="https:code.jquery.com/jquery-1.12.3.js">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#MyTable').DataTable( {
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            //to select and search from grid
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        } );
    } );
</script>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>

<!--jquery-ui-->
<script src="js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>

<script src="js/jquery-migrate.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>

<!--Nice Scroll-->
<script src="js/jquery.nicescroll.js" type="text/javascript"></script>

<!--right slidebar-->
<script src="js/slidebars.min.js"></script>

<!--switchery-->
<script src="js/switchery/switchery.min.js"></script>
<script src="js/switchery/switchery-init.js"></script>

<!--flot chart -->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/flot-spline.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="js/flot-chart/jquery.flot.pie.js"></script>

<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.crosshair.js"></script>


<!--earning chart init-->
<script src="js/earning-chart-init.js"></script>


<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--easy pie chart-->
<script src="js/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="js/easy-pie-chart.js"></script>


<!--vectormap-->
<script src="js/vector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/vector-map/jquery-jvectormap-world-mill-en.js"></script>
<script src="js/dashboard-vmap-init.js"></script>

<!--Icheck-->
<script src="js/icheck/skins/icheck.min.js"></script>
<script src="js/todo-init.js"></script>

<!--jquery countTo-->
<script src="js/jquery-countTo/jquery.countTo.js"  type="text/javascript"></script>

<!--owl carousel-->
<script src="js/owl.carousel.js"></script>

<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>


<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>

<!--common scripts for all pages-->

<script src="js/scripts.js"></script>


<script type="text/javascript">

    $(document).ready(function() {

        //countTo

        $('.timer').countTo();

        //owl carousel

        $("#news-feed").owlCarousel({
            Navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
        });
    });
    $(window).on("resize",function(){
        var owl = $("#news-feed").data("owlCarousel");
        owl.reinit();
    });
</script>
{{--<footer>--}}
{{--2018&copy; Log Out Technologies.--}}
{{--</footer>--}}
</body>
<!-- Mirrored from thevectorlab.net/slicklab/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Mar 2018 05:09:59 GMT -->
</html>
