<html>
<head>
    <title> welcome to guna</title>

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>

    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>
    <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
    <script>
        // Initialize Firebase

        var config = {
            apiKey: "AIzaSyDU8IaDjzwhcHfDOCcXFamIkPZWKfeFk1M",
            authDomain: "laravelwithfirebase-6ae34.firebaseapp.com",
            databaseURL: "https://laravelwithfirebase-6ae34.firebaseio.com",
            projectId: "laravelwithfirebase-6ae34",
            storageBucket: "laravelwithfirebase-6ae34.appspot.com",
            messagingSenderId: "871324337451"
        };


        firebase.initializeApp(config);
    </script>
</head>
<body>

<button id="myBtn">Branch</button>
<div id="myModal" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h3> Magme Admission: Branch </h3>
        </div>
        <div class="modal-body">
            <label for="fname">Event Id</label>
            <input type="text" name="id" id="user_id" required/>
            <label for="lname">Event Name </label>
            <input type="text" name="eventName" id="eventName" required/>
            <label for="lname">Select Branch</label>
            <input type="text" name="toBranch" id="toBranch" required/>
            <label for="lname">Event Date</label>
            <input type="date" name="eventDate" id="eventDate" required/>
            <label for="lname">Event Description</label>
            <input type="text" name="eventDesc" id="eventDesc" required/>
            <input type="button" class="button_submit" value="Save" onclick="save_user();" />
            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        </div>
    </div>

</div>

<script>

    var tblevents = document.getElementById('tbl_events_list');
    var databaseRef = firebase.database().ref('events/');
    var rowIndex = 1;

    databaseRef.once('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();

            var row = tblevents.insertRow(rowIndex);
            var cellId = row.insertCell(0);
            var celleventName = row.insertCell(1);
            var celltoBranch = row.insertCell(2);
            var celleventDate = row.insertCell(3);
            var celleventDesc = row.insertCell(4);

            cellId.appendChild(document.createTextNode(childKey));
            celleventName.appendChild(document.createTextNode(childData.eventName));
            celltoBranch.appendChild(document.createTextNode(childData.toBranch));
            celleventDate.appendChild(document.createTextNode(childData.eventDate));
            celleventDesc.appendChild(document.createTextNode(childData.eventDesc));


            rowIndex = rowIndex + 1;
        });
    });

    function save_user(){
        var eventName = document.getElementById('eventName').value;
        var toBranch = document.getElementById('toBranch').value;
        var eventDate = document.getElementById('eventDate').value;
        var eventDesc = document.getElementById('eventDesc').value;


        var uid = firebase.database().ref().child('events').push().key;

        var data = {
            user_id: uid,
            eventName: eventName,
            toBranch: toBranch,
            eventDate: eventDate,
            eventDesc: eventDesc,

        }

        var updates = {};
        updates['/events/' + uid] = data;
        firebase.database().ref().update(updates);

        alert('The user is created successfully!');
        reload_page();
    }

    function update_user(){
        var eventName = document.getElementById('eventName').value;
        var toBranch = document.getElementById('toBranch').value;
        var eventDate = document.getElementById('eventDate').value;
        var eventDesc = document.getElementById('eventDesc').value;
        var user_id = document.getElementById('user_id').value;

        var data = {
            user_id: user_id,
            eventName: eventName,
            toBranch: toBranch,
            eventDate: eventDate,
            eventDesc: eventDesc
        }

        var updates = {};
        updates['/events/' + user_id] = data;
        firebase.database().ref().update(updates);

        alert('The user is updated successfully!');

        reload_page();
    }

    function delete_user(){
        var user_id = document.getElementById('user_id').value;

        firebase.database().ref().child('/events/' + user_id).remove();
        alert('The user is deleted successfully!');
        reload_page();
    }

    function reload_page(){
        window.location.reload();
    }

</script>

<script>
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>


</body>
</html>




